# Lookup server and client

This application implements a small key-value cache server and client pair in front of an SQL Database.

For more details, see the READMEs at the client and server folders.

## Compilation

The application relies on code generation to normalize the request JSON to a string format that remains the same no matter in which order are the key columns specified. This string is used as the cache key.

Generated code is commited to the repo, so you don't need to generate again just in order to compile the app. But if you want to modify the request format, you will have to regenerate the JSON marshaler code.

In order to do so:

- Install golang [easyjson](https://github.com/mailru/easyjson) library

```bash
go get -u github.com/mailru/easyjson/...
```

- Add `${GOPATH}/bin` to your PATH

- Run `go generate` before building your modified app
