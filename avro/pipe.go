package avro

import (
	"context"
	"io"
)

// Pipe stdin to stdout, looking up the data in the process.
// Extends the schema from stdin to stdout, adding the specified fields as nullable avro objects.
// It will spawn as many parallel goroutines as there are tasks in the tasks slice.
func Pipe(r io.Reader, w io.Writer, extend map[string]Type, tasks []Task) error {
	ar, err := newReader(r)
	if err != nil {
		return err
	}
	schema := ar.Schema().Add(extend).String()
	aw, err := newWriter(schema, ar.CompressionName(), w, tasks)
	if err != nil {
		return err
	}
	defer aw.Close()
	aw.OpenBlock()
	ctx := context.Background()
	for ar.Next() {
		record, err := ar.Get()
		if err != nil {
			return err
		}
		aw.Add(ctx, record)
		if ar.LastInBlock() {
			if err := aw.CloseBlock(); err != nil {
				return err
			}
			aw.OpenBlock()
		}
	}
	if err := aw.CloseBlock(); err != nil {
		return err
	}
	if err := ar.Err(); err != nil {
		return err
	}
	return nil
}
