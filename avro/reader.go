package avro

import (
	json "encoding/json"
	"io"

	"github.com/pkg/errors"
	// Will have to use this forked version until goavro merges the support for logicalTypes
	goavro "github.com/saffron-digital/goavro"
)

// recordReader is an avro stream recordReader that automatically adapts
// the schema to the request and mapping
type recordReader struct {
	schema recordSchema
	reader *goavro.OCFReader
}

// newReader returns a new AvroReader
func newReader(in io.Reader) (*recordReader, error) {
	r, err := goavro.NewOCFReader(in)
	if err != nil {
		return nil, err
	}
	result := &recordReader{reader: r}
	schema := []byte(r.Codec().Schema()) // CanonicalSchema removes logical types
	if err := json.Unmarshal(schema, &(result.schema)); err != nil {
		return nil, err
	}
	return result, nil
}

// Next returns true if there are still records to read
func (r *recordReader) Next() bool {
	return r.reader.Scan()
}

// Get the current record
func (r *recordReader) Get() (Record, error) {
	raw, err := r.reader.Read()
	if err != nil {
		return nil, errors.Wrap(err, "Failed to read next record")
	}
	data, ok := raw.(map[string]interface{})
	if !ok {
		return nil, errors.Wrapf(err, "Failed to decode record %v into map[string]interface{}", raw)
	}
	return Record(data), nil
}

// LastInBlock returns true if the current record is the last in block
func (r *recordReader) LastInBlock() bool {
	return r.reader.RemainingBlockItems() == 0
}

// CompressionName returns the name of the compression algorithm
func (r *recordReader) CompressionName() string {
	return r.reader.CompressionName()
}

// Err returns the last error
func (r *recordReader) Err() error {
	return r.reader.Err()
}

// Schema returns the input schema with the additional key.Output fields
func (r *recordReader) Schema() recordSchema {
	return r.schema
}
