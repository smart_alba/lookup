package avro

import (
	"fmt"

	"bitbucket.org/smart_alba/lookup"
	goavro "github.com/saffron-digital/goavro"
)

// Record type for generic avro input
type Record lookup.Record

// Decap extracts a field from an Avro record
func (r Record) Decap(name string) interface{} {
	// If the input type is an union (nullable, for instance),
	// the value is a golang map with a single key (the actual type
	// of the union)
	field, ok := r[name]
	if !ok {
		return nil
	}
	if field == nil {
		return nil
	}
	switch m := field.(type) {
	case map[string]interface{}:
		for _, v := range m {
			return v
		}
	default:
		return field
	}
	return nil
}

// Encap a value (json: string or float64) in an Avro record
func (r Record) Encap(name string, t Type, v interface{}) {
	switch t {
	case String:
		switch vt := v.(type) {
		case string:
			v = vt
		case []byte:
			v = string(vt)
		default:
			v = fmt.Sprintf("%s", vt)
		}
	case Int:
		if vi, err := lookup.ToInt(v, 32); err == nil {
			v = int32(vi)
		} else {
			v = nil
		}
	case Long:
		if vi, err := lookup.ToInt(v, 64); err == nil {
			v = vi
		} else {
			v = nil
		}
	case Float:
		if vf, err := lookup.ToFloat(v, 32); err == nil {
			v = float32(vf)
		} else {
			v = nil
		}
	case Double:
		if vf, err := lookup.ToFloat(v, 64); err == nil {
			v = vf
		} else {
			v = nil
		}
	}
	if v == nil {
		r[name] = nil
		return
	}
	r[name] = goavro.Union(string(t), v)
}
