package avro

import (
	json "encoding/json"
	"fmt"
	"log"
	// Will have to use this forked version until goavro merges the support for logicalTypes
)

// Type is any of the set of supported Avro Types
type Type string

// Tipos de datos Avro que se pueden transportar por JSON, que es
// como recibimos los resultados de los lookups
const (
	String Type = "string"
	Int    Type = "int"
	Long   Type = "long"
	Float  Type = "float"
	Double Type = "double"
)

// recordField represents one field in the schema
type recordField struct {
	Name    string          `json:"name"`
	Type    json.RawMessage `json:"type"`
	Default json.RawMessage `json:"default,omitempty"`
}

// recordSchema represents a parsed avro schema
type recordSchema struct {
	Name      string        `json:"name,omitempty"`
	Type      string        `json:"type,omitempty"`
	Namespace string        `json:"namespace,omitempty"`
	Fields    []recordField `json:"fields,omitempty"`
}

// Add fields to the Schema
func (s recordSchema) Add(extend map[string]Type) recordSchema {
	fields := s.Fields
outer:
	for k, t := range extend {
		schema := []byte(fmt.Sprintf(`["null", "%s"]`, t))
		// If the field already exists, leave the schema unchanged
		for _, f := range fields {
			if f.Name == k {
				continue outer
			}
		}
		fields = append(fields, recordField{Name: k, Type: schema, Default: []byte("null")})
	}
	s.Fields = fields
	return s
}

// String turns a schema into a string
func (s recordSchema) String() string {
	schema, err := json.Marshal(s)
	if err != nil {
		log.Fatal("Failed to encode schema , err: ", err)
	}
	return string(schema)
}
