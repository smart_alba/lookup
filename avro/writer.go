package avro

import (
	"context"
	"io"
	"sync"

	// Will have to use this forked version until goavro merges the support for logicalTypes
	goavro "github.com/saffron-digital/goavro"
)

// Task is a function that mutates a record
type Task func(context.Context, Record) (Record, error)

// recordTask wraps the data that a goroutine needs to run a Task
type recordTask struct {
	record Record
	ctx    context.Context
}

// recordBlock encapsulates a block of records
type recordBlock struct {
	records []interface{}
	lastErr error
	*sync.WaitGroup
}

// recordWriter encapsulates an OCFWriter
type recordWriter struct {
	writer  *goavro.OCFWriter
	queue   chan recordTask
	current recordBlock
	sync.Mutex
}

// newWriter returns a writer with the schema provided.
// The writer will run as many goroutines as there are tasks in the tasks slice.
func newWriter(schema, compressionName string, w io.Writer, tasks []Task) (*recordWriter, error) {
	writer, err := goavro.NewOCFWriter(goavro.OCFConfig{
		W:               w,
		CompressionName: compressionName,
		Schema:          schema,
	})
	if err != nil {
		return nil, err
	}
	queue := make(chan recordTask, len(tasks))
	rw := &recordWriter{
		writer: writer,
		queue:  queue,
		current: recordBlock{
			records: make([]interface{}, 0, 100),
		},
	}
	for _, task := range tasks {
		go func(task Task) {
			var err error
			for data := range queue {
				if err == nil {
					err = rw.run(data.ctx, task, data.record)
				}
				rw.current.Done()
			}
		}(task)
	}
	return rw, nil
}

// OpenBlock opens a new avro block
func (w *recordWriter) OpenBlock() {
	w.current.records = w.current.records[:0]
	w.current.lastErr = nil
	w.current.WaitGroup = &sync.WaitGroup{}
}

// Add runs a task and adds the resulting record to the current block
func (w *recordWriter) Add(ctx context.Context, record Record) {
	w.current.Add(1)
	w.queue <- recordTask{
		ctx:    ctx,
		record: record,
	}
}

// CloseBlock opens a new avro block
func (w *recordWriter) CloseBlock() error {
	w.current.Wait()
	if w.current.lastErr != nil {
		return w.current.lastErr
	}
	if len(w.current.records) > 0 {
		return w.writer.Append(w.current.records)
	}
	return nil
}

// Close ends all the goroutines
func (w *recordWriter) Close() {
	close(w.queue)
}

// Run the task and store the result
func (w *recordWriter) run(ctx context.Context, task Task, record Record) error {
	record, err := task(ctx, record)
	w.Lock()
	defer w.Unlock()
	if err != nil {
		w.current.lastErr = err
		return err
	}
	// Cast the record to plain map[string]interface{}, otherwise
	// the avro library complains to get a Record type
	w.current.records = append(w.current.records, map[string]interface{}(record))
	return nil
}
