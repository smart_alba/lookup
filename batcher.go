package lookup

import (
	"context"
	"log"
	"sync"

	"github.com/davecgh/go-spew/spew"
)

// Batcher is an utility struct that batches records for upserting
type batcher struct {
	inserter  Inserter
	querier   Conn
	batch     []Record
	batchSize int
	current   int
}

// Pool is a worker pool to enrich records before upserting them
type pool struct {
	raw     chan Record
	rich    chan Record
	err     chan error
	lastErr error
	sync.WaitGroup
}

// task to be run by the pool
type task func(Record) (Record, error)

// newBatcher creates a new batcher
func newBatcher(u Inserter, q Conn, batchSize int) *batcher {
	return &batcher{
		inserter:  u,
		querier:   q,
		batchSize: batchSize,
		batch:     make([]Record, 0, batchSize),
	}
}

// Add a record to the batch, push it if the batch is full
func (b *batcher) Add(ctx context.Context, r Record) error {
	b.batch = append(b.batch, r)
	b.current++
	if b.current < b.batchSize {
		return nil
	}
	return b.Flush(ctx)
}

// Flush the current batch
func (b *batcher) Flush(ctx context.Context) error {
	if len(b.batch) <= 0 {
		return nil
	}
	if err := b.flush(ctx, b.batch); err != nil {
		return err
	}
	b.batch = b.batch[:0]
	b.current = 0
	return nil
}

// flush a partial batch
func (b *batcher) flush(ctx context.Context, batch []Record) error {
	s := b.inserter(batch)
	if err := b.querier.ExecContext(ctx, s.Query, s.Params); err != nil {
		// Try again, in smaller batches
		batchSize := len(batch)
		splitSize := batchSize / 10
		if splitSize <= 0 {
			splitSize = 1
		}
		if splitSize >= batchSize {
			log.Printf("Failed to update record %s", spew.Sdump(batch[0]))
			return err
		}
		// Split the batch in blocks of splitSize records
		for base := 0; base < batchSize; base += splitSize {
			top := base + splitSize
			if top > batchSize {
				top = batchSize
			}
			if err := b.flush(ctx, batch[base:top]); err != nil {
				return err
			}
		}
	}
	return nil
}

// NewPool creates a pool of gophers to parallelize tasks before upserting records
func newPool(tasks []task) *pool {
	gophers := len(tasks)
	p := &pool{
		raw:  make(chan Record, 2*gophers),
		rich: make(chan Record, 2*gophers),
		err:  make(chan error, 2*gophers),
	}
	// Start gophers for the lookup tasks
	for i := gophers - 1; i >= 0; i-- {
		task := tasks[i]
		p.Add(1)
		go func() {
			defer p.Done()
			var err error
			for r := range p.raw {
				if r, err = task(r); err != nil {
					p.err <- err
					return
				}
				p.rich <- r
			}
		}()
	}
	return p
}

// Start the pool, waiting for data
func (p *pool) Start(ctx context.Context, b *batcher) {
	// Have the batcher waiting for input in the background
	go func() {
		defer close(p.err)
		var err error
		for record := range p.rich {
			if err = b.Add(ctx, record); err != nil {
				break
			}
		}
		if err != nil {
			p.err <- err
			// exhaust any pending record before leaving, in case of error
			for range p.rich {
			}
			return
		}
		if err = b.Flush(ctx); err != nil {
			p.err <- err
		}
	}()
}

// Add a record to the pool
func (p *pool) Push(record Record) error {
	select {
	case err := <-p.err:
		p.lastErr = err
		return err
	case p.raw <- record:
	}
	return nil
}

// Stop the pool
func (p *pool) Stop() {
	// Terminate lookup goroutines
	close(p.raw)
	// Wait until they are done
	p.Wait()
	// Then terminate push goroutine
	close(p.rich)
	// Wait til error channel is closed, to make sure all info has been committed
	for err := range p.err {
		p.lastErr = err
	}
}

// Err collects any error left after stopping the pool
func (p *pool) Err() error {
	return p.lastErr
}
