package main

import (
	"log"
	"path/filepath"
	"strings"
	"text/template"

	"bitbucket.org/smart_alba/lookup"
)

type fieldPair struct {
	Column string
	Field  string
}

// Split the flag list into pairs
func split(i []string, help string) []fieldPair {
	result := make([]fieldPair, 0, len(i))
	for _, each := range i {
		pair := strings.SplitN(each, ":", 2)
		if len(pair) < 2 {
			log.Fatalf("Invalid input '%s': Must have the format 'column:%s'", each, help)
		}
		result = append(result, fieldPair{
			Column: strings.TrimSpace(pair[0]),
			Field:  strings.TrimSpace(pair[1]),
		})
	}
	return result
}

// asMap turns a []string to a map[string]struct{}
func asMap(data []string) map[string]struct{} {
	result := make(map[string]struct{})
	for _, k := range data {
		result[k] = struct{}{}
	}
	return result
}

// AsField adds the set of options to the given map
func asField(i []string, exclusions map[string]struct{}, reg, exc map[string]string) {
	for _, pair := range split(i, "attribute") {
		m := reg
		if _, ok := exclusions[pair.Column]; ok {
			m = exc
		}
		m[pair.Column] = pair.Field
	}
}

// AsOutput adds the set of options to name and type maps
func asOutput(i []string, typeName string, exclusions map[string]struct{}, types, reg, exc map[string]string) {
	for _, pair := range split(i, "attribute") {
		m := reg
		if _, ok := exclusions[pair.Column]; ok {
			m = exc
		}
		types[pair.Column] = typeName
		m[pair.Column] = pair.Field
	}
}

// AsSort returs the input list as a slice of lookup.Sorting
func asSort(i []string) []lookup.Sorting {
	if len(i) <= 0 {
		return nil
	}
	pairs := split(i, "order (asc / desc)")
	order := make([]lookup.Sorting, 0, len(pairs))
	for _, pair := range pairs {
		desc := false
		switch strings.ToLower(pair.Field) {
		case "desc":
			desc = true
		case "asc":
			desc = false
		default:
			log.Fatalf("Alias %s not supported, can only be 'asc' or 'desc'", pair.Field)
		}
		order = append(order, lookup.Sorting{
			Field: pair.Column,
			Desc:  desc,
		})
	}
	return order
}

// Options includes the config options for the client
type Options struct {
	Template string   `yaml:"template" help:"Template file to modify lookup records"`
	Inputs   []string `yaml:"input" help:"Filter input pairs (column name:record field name)"`
	Tonly    []string `yaml:"tonly" help:"Template-only input and output fields not used for lookup"`
	OString  []string `yaml:"string" help:"String output pairs (column name:record field name)"`
	OInt     []string `yaml:"int" help:"Int output pairs (column name:record field name)"`
	OLong    []string `yaml:"long" help:"Long output pairs (column name:record field name)"`
	OFloat   []string `yaml:"float" help:"Float output pairs (column name:record field name)"`
	ODouble  []string `yaml:"double" help:"Double pairs (column name:record field name)"`
	Order    []string `yaml:"sort" help:"Sort input pairs (column name:asc or desc)"`
	CDC      []string `yaml:"cdc" help:"Change Data Capture fields (column name:record field that must be larger)"`
	Threads  int      `yaml:"threads" help:"Number of simultaneous goroutines"`
	Test     bool     `yaml:"test" help:"Run the template against input vars, using 'input' for testing it"`
	Table    string   `yaml:"table" help:"Name of the database table to query"`
	// These options when used for lookup
	URL string `yaml:"url" help:"URL Address of the lookup REST Server (e.g http://localhost:8080)"`
	// These options when used for CDC
	Driver string `yaml:"driver" help:"Name of go database driver"`
	DSN    string `yaml:"dsn" help:"Database DSN in golang format, user:password@tcp(host:port)/db"`
}

// Defaults values for Options
func Defaults() Options {
	return Options{
		Template: "",
		Driver:   "mysql",
		Threads:  8,
	}
}

func init() {
	lookup.InitConfig(Defaults())
}

func newMapping() (Options, Mapping) {
	opts := Defaults()
	if err := lookup.LoadConfig(&opts, "MAPPING"); err != nil {
		log.Fatal("Failed to read configuration options, ", err)
	}
	m := Mapping{
		Input:   make(map[string]string),
		Tinput:  make(map[string]string),
		Output:  make(map[string]string),
		Toutput: make(map[string]string),
		Types:   make(map[string]string),
	}
	tOnly := asMap(opts.Tonly)
	// Fill inputs from flags
	asField(opts.Inputs, tOnly, m.Input, m.Tinput)
	cdc := split(opts.CDC, "[!]attribute")
	if len(cdc) > 0 {
		m.CDC = make([]Change, len(cdc))
		for idx, c := range cdc {
			// By Default, CDC tracks incremental changes
			field, ctype := c.Field, IncrementalChange
			// But if the field name is prefixed by "!", it tracks status changes
			if strings.HasPrefix(field, "!") {
				field = strings.TrimPrefix(field, "!")
				ctype = StateChange
			}
			m.CDC[idx].Column = c.Column
			m.CDC[idx].Field = field
			m.CDC[idx].ChangeType = ctype
		}
	}
	// fill order from flags
	m.Order = asSort(opts.Order)
	// fill outputs from flags
	for _, f := range []struct {
		name  string
		flags []string
	}{
		{name: "string", flags: opts.OString},
		{name: "int", flags: opts.OInt},
		{name: "long", flags: opts.OLong},
		{name: "float", flags: opts.OFloat},
		{name: "double", flags: opts.ODouble},
	} {
		asOutput(f.flags, f.name, tOnly, m.Types, m.Output, m.Toutput)
	}
	// Check if there is a template
	if opts.Template != "" {
		base := filepath.Base(opts.Template)
		m.template = template.Must(template.New(base).Funcs(lookup.FuncMap()).ParseFiles(opts.Template))
	}
	return opts, m
}
