package main

import (
	"os"
	"testing"

	"bitbucket.org/smart_alba/lookup"
	"github.com/google/go-cmp/cmp"
)

func TestFlags(t *testing.T) {
	oldArgs := os.Args
	defer func() { os.Args = oldArgs }()
	os.Args = []string{"client",
		"-input", "incol1:inattrib1", "-input", "incol 2: inattrib 2",
		"-input", "lookup:manattrib1", "-input", "mancol 2: manattrib 2",
		"-string", "scol1:sattrib1", "-string", "scol 2: sattrib 2",
		"-int", "icol1:iattrib1", "-int", "icol 2: iattrib 2",
		"-long", "lcol1:lattrib1", "-long", "lcol 2: lattrib 2",
		"-float", "fcol1:fattrib1", "-float", "fcol 2: fattrib 2",
		"-double", "dcol1:dattrib1", "-double", "dcol 2: dattrib 2",
		"-sort", "scol1:asc", "-sort", "scol 2: desc",
	}
	expected := Mapping{
		Input: map[string]string{
			"incol1":   "inattrib1",
			"incol 2":  "inattrib 2",
			"lookup":   "manattrib1",
			"mancol 2": "manattrib 2",
		},
		Output: map[string]string{
			"scol1": "sattrib1", "scol 2": "sattrib 2",
			"icol1": "iattrib1", "icol 2": "iattrib 2",
			"lcol1": "lattrib1", "lcol 2": "lattrib 2",
			"fcol1": "fattrib1", "fcol 2": "fattrib 2",
			"dcol1": "dattrib1", "dcol 2": "dattrib 2",
		},
		Types: map[string]string{
			"scol1": "string", "scol 2": "string",
			"icol1": "int", "icol 2": "int",
			"lcol1": "long", "lcol 2": "long",
			"fcol1": "float", "fcol 2": "float",
			"dcol1": "double", "dcol 2": "double",
		},
		Order: []lookup.Sorting{
			{Field: "scol1"}, {Field: "scol 2", Desc: true},
		},
	}
	_, m := newMapping()
	if !cmp.Equal(m.Input, expected.Input) {
		t.Errorf("Input Expected %v, got %v", expected.Input, m.Input)
	}
	if !cmp.Equal(m.Output, expected.Output) {
		t.Errorf("Output Expected %v, got %v", expected.Output, m.Output)
	}
	if !cmp.Equal(m.Types, expected.Types) {
		t.Errorf("Types Expected %v, got %v", expected.Types, m.Types)
	}
	if !cmp.Equal(m.Order, expected.Order) {
		t.Errorf("Order Expected %v, got %v", expected.Order, m.Order)
	}
}
