package main

import (
	"fmt"
	"log"
	"os"

	"bitbucket.org/smart_alba/lookup"
	"bitbucket.org/smart_alba/lookup/avro"
	"bitbucket.org/smart_alba/lookup/test"
	_ "github.com/go-sql-driver/mysql"
)

func main() {

	opts, mapping := newMapping()
	if opts.Test {
		record := make(lookup.Record)
		for k, v := range mapping.Input {
			record[k] = v
		}
		fmt.Println("TEMPLATE: ")
		if err := mapping.template.Execute(os.Stdout, record); err != nil {
			fmt.Printf("ERROR: %+v\n", err)
		}
		fmt.Printf("RECORD: %+v\n", record)
		os.Exit(0)
	}

	if opts.Table == "" {
		log.Fatal("Table cannot be empty")
	}
	if opts.Threads <= 0 {
		opts.Threads = 8
	}

	var cache lookup.Interface
	if opts.URL != "" && len(mapping.CDC) <= 0 {
		cache = lookup.FromHTTP(opts.URL)
	} else if opts.DSN == "" || opts.Driver == "" {
		log.Fatal("Must either provide URL or DSN")
	} else {
		db, quoter := test.Must(opts.Driver, opts.DSN)
		cache = lookup.Memoize(lookup.FromDB(db, quoter))
	}

	// Prepare the cache and tasks
	key := mapping.Key()
	key.Table = opts.Table
	// Create as many parallel tasks as needed
	tasks := make([]avro.Task, 0, opts.Threads)
	for i := opts.Threads; i > 0; i-- {
		tasks = append(tasks, mapping.Factory(cache, key))
	}

	// Run the pipe
	err := avro.Pipe(os.Stdin, os.Stdout, mapping.Extend(), tasks)
	if err != nil {
		log.Fatal(err)
	}
}
