package main

import (
	"context"
	"io"
	"io/ioutil"
	"text/template"

	"bitbucket.org/smart_alba/lookup"
	"bitbucket.org/smart_alba/lookup/avro"
	"github.com/pkg/errors"
)

// MatchKey is the attribute added to a record to flag whether it matches or not
const MatchKey string = "lookup_match"

// Mapping from database table names to record fields and avro field types.
// Only simple scalar types (string, int, long, float, double) are currently supported.
type Mapping struct {
	Input    map[string]string
	Tinput   map[string]string
	Output   map[string]string
	Toutput  map[string]string
	Types    map[string]string
	CDC      []Change
	Order    []lookup.Sorting
	template *template.Template
}

// Change describes how to look for changes between the record and the value cached in the CDC
type Change struct {
	// Column name in the cache table
	Column string
	// Field name in the record
	Field string
	// ChangeType encapsulates the change decission ina CDC function. A record is an update over a previous value if this function returns true.
	ChangeType func(recordVal, cacheVal int64) bool
}

// IncrementalChange is a CDC Type that checks for increases in the cdc value
func IncrementalChange(recordVal, cacheVal int64) bool {
	return recordVal > cacheVal
}

// StateChange is a CDCType that looks for any changes in the cdc value
func StateChange(recordVal, cacheVal int64) bool {
	return recordVal != cacheVal
}

// Extend returns the set of names and types to extend the avro schema
func (m *Mapping) Extend() map[string]avro.Type {
	result := make(map[string]avro.Type)
	for _, mm := range []map[string]string{m.Output, m.Toutput} {
		for k, a := range mm {
			result[a] = avro.Type(m.Types[k])
		}
	}
	result[MatchKey] = avro.Int
	return result
}

// Key made from the mapping parameters
func (m *Mapping) Key() lookup.Request {
	input := make([]string, 0, len(m.Input))
	param := make([]interface{}, len(m.Input))
	output := make([]string, 0, len(m.Output)+len(m.CDC))
	for k := range m.Input {
		input = append(input, k)
	}
	for k := range m.Output {
		output = append(output, k)
	}
	for _, k := range m.CDC {
		output = append(output, k.Column)
	}
	key := lookup.Request{
		Input:  input,
		Param:  param,
		Output: output,
		Order:  m.Order,
	}
	key.Sort()
	return key
}

// Factory of tasks for the lookup job
func (m *Mapping) Factory(cache lookup.Interface, key lookup.Request) avro.Task {
	// allocate a param array per goroutine
	key.Param = make([]interface{}, len(key.Input))
	buffer := make(lookup.Record)
	return avro.Task(func(ctx context.Context, record avro.Record) (avro.Record, error) {
		if err := m.lookup(ctx, cache, record, key, buffer); err != nil {
			return nil, err
		}
		return record, nil
	})
}

// Lookup the record in the cache and update the values
func (m *Mapping) lookup(ctx context.Context, cache lookup.Interface, record avro.Record, key lookup.Request, buffer lookup.Record) error {
	// Update the buffer record from the avro input
	record.Encap(MatchKey, avro.Int, 0)
	for _, k := range key.Input {
		buffer[k] = record.Decap(m.Input[k])
	}
	// If there is a template, apply to the record
	if m.template != nil {
		for k, a := range m.Tinput {
			buffer[k] = record.Decap(a)
		}
		if err := m.template.Execute(ioutil.Discard, buffer); err != nil {
			return errors.Wrapf(err, "Failed to execute template on record %+v mapped to %+v", record, buffer)
		}
		for k, a := range m.Toutput {
			record.Encap(a, avro.Type(m.Types[k]), buffer[k])
		}
	}
	// Build the key from the buffering record
	param := key.Param[:0]
	for _, k := range key.Input {
		param = append(param, buffer[k])
	}
	if len(param) <= 0 {
		return nil
	}
	key.Param = param
	// Lookup the key!
	h, err := cache.Get(ctx, key, buffer)
	if err != nil {
		if errors.Cause(err) == io.EOF {
			// If key is not found, but we are doing a CDC request
			// then this is a match.
			if len(m.CDC) > 0 {
				record.Encap(MatchKey, avro.Int, 1)
			}
			return nil
		}
		return err
	}
	result, err := h.Native()
	if err != nil {
		return err
	}
	for k, a := range m.Output {
		record[a] = nil
		if v, ok := result[k]; ok && v != nil {
			record.Encap(a, avro.Type(m.Types[k]), v)
		}
	}
	if len(m.CDC) > 0 {
		ok, err := m.checkCDC(result, record)
		if err != nil {
			return err
		}
		if !ok {
			return nil
		}
	}
	record.Encap(MatchKey, avro.Int, 1)
	return nil
}

// Check if the record matched the Changed Data Capture requirements
func (m *Mapping) checkCDC(result map[string]interface{}, record avro.Record) (bool, error) {
	for _, cdc := range m.CDC {
		current := record.Decap(cdc.Field)
		if current == nil {
			return false, nil
		}
		// Turn the record value to an int
		rint, err := lookup.ToInt(current, 64)
		if err != nil {
			return false, err
		}
		// Turn the result value to an int, too
		lint, err := lookup.ToInt(result[cdc.Column], 64)
		if err != nil {
			return false, err
		}
		// If record is newer than lookup, return true
		if cdc.ChangeType(rint, lint) {
			return true, nil
		}
	}
	return false, nil
}
