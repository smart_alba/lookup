package main

import (
	"context"
	"io"
	"testing"
	"text/template"

	"bitbucket.org/smart_alba/lookup"
	"bitbucket.org/smart_alba/lookup/avro"
	"github.com/google/go-cmp/cmp"
	"github.com/pkg/errors"
)

var tmplString string

func init() {
	tmplString = `
{{ .Encap "calculated" (add (.Decap "basecol") 1) }}
// Separo por puntos, barras o paréntesis (Palestina. Estado Observado)
{{ $first := regexSplit "[\\/\\(\\.]+" ($.Decap "lookup") -1 | first | trim }}
{{ $.Encap "lookup" "" }}
// Separo por espacios y comas (Palmas, Las)
{{ range regexSplit "[\\s,]+" $first -1 }}
	// Me quedo solo con palabras de mas de 2 letras
	{{ if gt (len .) 2 }}
		// Y elimino caracteres no ASCII
		{{ $ascii := regexReplaceAllLiteral "[^a-zA-Z0-9]+" . "" | trim }}
		{{ $.Concat "lookup" (lower $ascii) }}
	{{ end }}
{{ end }}
// Si el texto es "pases" o "paises", dejo en blanco
{{ if eq ($.Decap "lookup") "pases" "paises" }}
	{{ .Encap "lookup" "" }}
{{ end }}
`
}

func testMapping() Mapping {
	return Mapping{
		Input: map[string]string{
			"incol1": "inattrib1",
			"lookup": "manattrib1",
		},
		Tinput: map[string]string{
			"basecol": "baseattr",
		},
		Output: map[string]string{
			"scol1": "sattrib1", "scol 2": "sattrib 2",
			"icol1": "iattrib1", "lcol 2": "lattrib 2",
		},
		Toutput: map[string]string{
			"calculated": "calculated",
		},
		Types: map[string]string{
			"scol1": "string", "scol 2": "string",
			"icol1": "int", "lcol 2": "long",
			"calculated": "int",
		},
		Order: []lookup.Sorting{
			{Field: "scol1"}, {Field: "scol 2", Desc: true},
		},
	}
}

func TestExtend(t *testing.T) {
	m := Mapping{
		Output: map[string]string{
			"o1": "alias1",
			"o2": "alias 2",
		},
		Types: map[string]string{
			"o1": "string",
			"o2": "long",
		},
	}
	expected := map[string]avro.Type{
		"alias1":       "string",
		"alias 2":      "long",
		"lookup_match": "int",
	}
	if e := m.Extend(); !cmp.Equal(e, expected) {
		t.Errorf("Expected %v, got %v", expected, e)
	}
}

func TestKey(t *testing.T) {
	m := testMapping()
	expected := lookup.Request{
		Input:  []string{"incol1", "lookup"},
		Output: []string{"icol1", "lcol 2", "scol 2", "scol1"},
		Param:  make([]interface{}, len(m.Input)),
		Order:  m.Order,
	}
	if k := m.Key(); !cmp.Equal(k, expected) {
		t.Errorf("Expected %v, got %v", expected, k)
	}
}

type fixedLookup []byte

type failedLookup struct {
	err error
}

func (f fixedLookup) Get(ctx context.Context, key lookup.Request, out lookup.Record) (lookup.Hit, error) {
	return lookup.ByteHit([]byte(f)), nil
}

func (f failedLookup) Get(ctx context.Context, key lookup.Request, out lookup.Record) (lookup.Hit, error) {
	return nil, f.err
}

func TestTaskUpdatesRecord(t *testing.T) {
	m := testMapping()
	record := avro.Record(map[string]interface{}{
		// a nullable attrib of type int
		"inattrib1": map[string]interface{}{
			"int": 5,
		},
		// a non-nullable atrib of type string
		"manattrib1": "Coruña, La",
	})
	cache := fixedLookup([]byte(`{ "icol1": 6, "lcol 2": 7, "scol1": "a", "scol 2": "b" }`))
	r, err := m.Factory(cache, m.Key())(context.Background(), record)
	if err != nil {
		t.Fatal(err)
	}
	expected := avro.Record(map[string]interface{}{
		"inattrib1":    record["inattrib1"],
		"manattrib1":   record["manattrib1"],
		"iattrib1":     map[string]interface{}{"int": int32(6)},
		"lattrib 2":    map[string]interface{}{"long": int64(7)},
		"sattrib1":     map[string]interface{}{"string": "a"},
		"sattrib 2":    map[string]interface{}{"string": "b"},
		"lookup_match": map[string]interface{}{"int": int32(1)},
	})
	if !cmp.Equal(expected, r) {
		t.Errorf("Expected %v, got %v, diff: %s", expected, r, cmp.Diff(expected, r))
	}
}

func TestTaskLookupEOFIsNotMatch(t *testing.T) {
	m := testMapping()
	record := avro.Record(map[string]interface{}{
		// a nullable attrib of type int
		"inattrib1": map[string]interface{}{
			"int": 5,
		},
		// a non-nullable atrib of type string
		"manattrib1": "Coruña, La",
	})
	cache := failedLookup{err: io.EOF}
	r, err := m.Factory(cache, m.Key())(context.Background(), record)
	if err != nil {
		t.Fatal(err)
	}
	if r.Decap(MatchKey).(int32) != 0 {
		t.Error("Expected failed match, got match")
	}
}

func TestTaskLookupErrorIsPropagated(t *testing.T) {
	m := testMapping()
	record := avro.Record(map[string]interface{}{
		// a nullable attrib of type int
		"inattrib1": map[string]interface{}{
			"int": 5,
		},
		// a non-nullable atrib of type string
		"manattrib1": "Coruña, La",
	})
	newErr := errors.New("Test Error")
	cache := failedLookup{err: newErr}
	_, err := m.Factory(cache, m.Key())(context.Background(), record)
	if err == nil {
		t.Fail()
	}
	if errors.Cause(err) != newErr {
		t.Error("Expected custom error, got a different one")
	}
}

func TestTaskTemplate(t *testing.T) {
	m := testMapping()
	m.template = template.Must(template.New("test").Funcs(lookup.FuncMap()).Parse(tmplString))
	record := avro.Record(map[string]interface{}{
		// a nullable attrib of type int
		"inattrib1": map[string]interface{}{
			"int": 5,
		},
		// a non-nullable atrib of type string
		"manattrib1": "Coruña, La",
		// Template input record
		"baseattr": 10,
	})
	cache := fixedLookup([]byte(`{ "icol1": 6, "lcol 2": 7, "scol1": "a", "scol 2": "b" }`))
	r, err := m.Factory(cache, m.Key())(context.Background(), record)
	if err != nil {
		t.Fatal(err)
	}
	c := r.Decap("calculated")
	if c == nil {
		t.Fatal("Expected calculated field to be present")
	}
	if c.(int32) != 11 {
		t.Errorf("Expected calculated field to be 11, got %+v", c)
	}
}
