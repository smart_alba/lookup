package lookup

import (
	"flag"
	"fmt"
	"log"
	"os"
	"reflect"
	"strconv"
	"strings"

	yaml "gopkg.in/yaml.v2"
)

// ListVar is a flag variable that can hold a comma-separated list of items
type listVar struct {
	Each  []string `yaml:"each"`
	isSet bool
}

// Set implements flag.Var
func (i *listVar) Set(value string) error {
	// Allows overwriting defaults
	if !i.isSet {
		i.Each, i.isSet = nil, true
	}
	split := strings.Split(value, ",")
	for _, item := range split {
		i.Each = append(i.Each, strings.TrimSpace(item))
	}
	return nil
}

// String implements flag.Var
func (i *listVar) String() string {
	return strings.Join(i.Each, ",")
}

// InitConfig adds config flags, to be called on init() function, based on reflection on the provided struct
func InitConfig(opts interface{}) {
	ptr, val := reflect.TypeOf(opts), reflect.ValueOf(opts)
	if ptr.Kind() == reflect.Ptr {
		ptr = ptr.Elem()
		val = val.Elem()
	}
	if val.Kind() != reflect.Struct {
		log.Fatalf("Input parameter must be struct or pointer to struct, not %+v", val.Kind())
	}
	for i := ptr.NumField() - 1; i >= 0; i-- {
		field := ptr.Field(i)
		name := strings.SplitN(field.Tag.Get("yaml"), ",", 2)[0]
		if name == "" {
			log.Fatalf("No yaml tag in exported config field %s", field.Name)
		}
		help := field.Tag.Get("help")
		switch dval := val.Field(i).Interface().(type) {
		case string:
			flag.String(name, dval, help)
		case int:
			flag.Int(name, dval, help)
		case uint:
			flag.Uint(name, dval, help)
		case bool:
			flag.Bool(name, dval, help)
		case []string:
			flag.Var(&listVar{Each: dval}, name, help)
		default:
			log.Fatalf("Unsupported flag type %+v", field.Type)
		}
	}
	flag.String("c", "", "Config file name")
}

// LoadConfig fills a config struct from flags, environment or file
func LoadConfig(opts interface{}, envPrefix string) error {
	flag.Parse()
	// If there is a config file name provided, use it
	cfile := flag.Lookup("c")
	if cfile != nil {
		cname := cfile.Value.String()
		if cname != "" {
			f, err := os.Open(cname)
			if err != nil {
				return err
			}
			defer f.Close()
			dec := yaml.NewDecoder(f)
			if err := dec.Decode(opts); err != nil {
				return err
			}
		}
	}
	ptr := reflect.TypeOf(opts)
	if ptr.Kind() != reflect.Ptr {
		log.Fatalf("Input parameter must be pointer to struct, not %+v", ptr.Kind())
	}
	elem, val := ptr.Elem(), reflect.ValueOf(opts).Elem()
	v := visited()
	for i := elem.NumField() - 1; i >= 0; i-- {
		name := strings.SplitN(elem.Field(i).Tag.Get("yaml"), ",", 2)[0]
		mergeFlag(envPrefix, v, name, val.Field(i))
	}
	return nil
}

// ReadFlags gets all flags with non default values
func visited() map[string]bool {
	visited := make(map[string]bool)
	flag.Visit(func(f *flag.Flag) {
		visited[f.Name] = true
	})
	return visited
}

// isZero returns true if the value equals the zero value for its type
func isZero(v reflect.Value) bool {
	t, i := v.Type(), v.Interface()
	switch t.Kind() {
	case reflect.Ptr, reflect.Slice, reflect.Map, reflect.Interface:
		if v.IsNil() {
			return true
		}
	}
	return t.Comparable() && i == reflect.Zero(t).Interface()
}

// MergeFlag merges environment and command line values with config
func mergeFlag(prefix string, visited map[string]bool, name string, val reflect.Value) {
	envName := fmt.Sprintf("%s_%s", strings.ToUpper(prefix), strings.ToUpper(name))
	e := os.Getenv(envName)
	f := flag.Lookup(name)
	if f != nil {
		if f := f.Value.String(); f != "" {
			// skip defaults, if the item has some value already
			if visited[name] || !val.IsValid() || isZero(val) {
				e = f
			}
		}
	}
	if e == "" {
		return
	}
	switch val.Kind() {
	case reflect.String:
		val.SetString(e)
	case reflect.Int:
		v, err := strconv.ParseInt(e, 10, 32)
		if err != nil {
			log.Fatalf("Failed to parse environment variable %s value %s as an int, %+v", envName, e, err)
		}
		val.SetInt(v)
	case reflect.Uint:
		v, err := strconv.ParseUint(e, 10, 32)
		if err != nil {
			log.Fatalf("Failed to parse environment variable %s value %s as an uint, %+v", envName, e, err)
		}
		val.SetUint(v)
	case reflect.Bool:
		e = strings.ToLower(e)
		r := false
		if e == "t" || e == "true" || e == "y" || e == "yes" || e == "s" || e == "si" || e == "sí" || e == "1" {
			r = true
		}
		val.SetBool(r)
	case reflect.Slice:
		trimmed := make([]string, 0, 8)
		for _, v := range strings.Split(e, ",") {
			trimmed = append(trimmed, strings.TrimSpace(v))
		}
		val.Set(reflect.ValueOf(trimmed))
	default:
		log.Fatalf("Unsupported flag type %+v", val.Type())
	}
}
