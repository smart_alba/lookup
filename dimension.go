package lookup

import (
	"context"
	"io/ioutil"
	"sync"
	"text/template"

	"github.com/pkg/errors"
)

// UpdateGophers is the number of update gopher threads
const updateGophers = 8

// Dimension describes how to populate a dimension from a source table
type Dimension struct {
	// Table to store the dimension into
	Table string `yaml:"table"`
	// Mapping dimension field => source table attribute
	Mapping map[string]string `yaml:"mapping"`
	// SkipUpdate flags a particular dimension to not be updated, just looked up
	SkipUpdate bool `yaml:"skipUpdate,omitempty"`
	// ID field - primary key
	ID string `yaml:"id,omitempty"`
	// Hash - If true, ID field is generated from a hash of type 2 attributes.
	Hash bool `yaml:"hash,omitempty"`
	// Order to sort the dimension fields
	Order []Sorting `yaml:"order,omitempty"`
	// Type 2 attributes
	Type2 []string `yaml:"type2"`
	// Type 1 attributes
	Type1 []string `yaml:"type1"`
	// Updates to perform on type1 fields
	Updates []Update `yaml:"updates,omitempty"`
	// Task to fill required fields to update the dimension, as a golang template.
	// Only used for its side effects, the template output is actually discarded
	Task string `yaml:"task,omitempty"`
	// Dimensions is a list of nested dimensions
	Dimensions map[string]Dimension `yaml:"dimensions,omitempty"`
	// Task, compiled
	task *template.Template
}

// Source table to update the star from
type Source interface {
	// Partition builds the SELECT ... PARTITION BY using a map to
	// translate from expected column name to source name
	Partition(ctx context.Context, q Querier, quoter Quoter, uniq, attr []string) (QueryResult, error)
}

// Compile tasks for each dimension in the star
func (dim *Dimension) Compile(name string) error {
	if dim.Task != "" {
		tmpl, err := template.New(name).Funcs(FuncMap()).Parse(dim.Task)
		if err != nil {
			return errors.Wrapf(err, "Failed to decode template %s", dim.Task)
		}
		dim.task = tmpl
	}
	for key, nested := range dim.Dimensions {
		nested.Compile(key)
		// Overwrite with compiled item
		dim.Dimensions[key] = nested
	}
	return nil
}

// Transfer data from a source table to a cube
func (dim *Dimension) Transfer(ctx context.Context, in Querier, out Connecter, qin, qout Quoter, cache Interface, s Source) error {
	// Read nested dimensions first
	if len(dim.Dimensions) > 0 {
		if err := dim.nestedTransfer(ctx, in, out, qin, qout, cache, s); err != nil {
			return err
		}
	}
	// Build a connection for refresh, updates
	c, err := out.Connection(ctx)
	if err != nil {
		return err
	}
	defer c.Close()
	// Get all points, including nested dimensions coordinates
	if !dim.SkipUpdate {
		if err := dim.upsert(ctx, in, c, qin, qout, cache, s); err != nil {
			return err
		}
	}
	// Add also local updates
	if len(dim.Updates) <= 0 {
		return nil
	}
	for _, upd := range dim.Updates {
		// Don't parallelize updates, since they are all on the same table
		if err := dim.refresh(ctx, in, c, qin, qout, upd, s); err != nil {
			return err
		}
	}
	return nil
}

// Upsert updates the specified dimension
func (dim *Dimension) upsert(ctx context.Context, in Querier, q Conn, qin, qout Quoter, cache Interface, s Source) error {
	points, err := dim.points(ctx, in, qin, s)
	if err != nil {
		return err
	}
	defer points.Close()
	// Build a pool of gophers for lookups
	tasks := make([]task, 0, updateGophers)
	for i := updateGophers; i > 0; i-- {
		keys := dim.nestedKeys()
		tasks = append(tasks, func(r Record) (Record, error) {
			// Perform the nested lookups first, before translating
			// (each nested dimension may perform its own translations,
			// will require the original field names)
			r, err := dim.nestedLookup(ctx, cache, r, keys)
			if err != nil {
				return nil, err
			}
			if err := dim.translate(r, r); err != nil {
				return r, err
			}
			return r, nil
		})
	}
	p := newPool(tasks)
	// Keep adding tasks to the pool
	p.Start(ctx, newBatcher(dim.inserter(qout), q, 100))
	for points.Next() {
		r := make(Record)
		if err = points.Scan(r); err != nil {
			break
		}
		if err = p.Push(r); err != nil {
			break
		}
	}
	p.Stop()
	// Check for errors
	if err != nil {
		return err
	}
	if err := points.Err(); err != nil {
		return err
	}
	if err := p.Err(); err != nil {
		return err
	}
	return nil
}

// lookup a dimension type 2 attribs, return ID
func (dim *Dimension) lookup(ctx context.Context, cache Interface, r Record, key Request) (interface{}, error) {
	if !dim.Hash {
		// ID is not a hash, but a calculated field. We don't need to do a db lookup.
		if id, ok := r[dim.ID]; ok {
			return id, nil
		}
		return nil, errors.Errorf("ID Field %s of dim %s is missing in record %+v ", dim.ID, dim.Table, r)
	}
	param := key.Param[:0]
	for _, k := range key.Input {
		param = append(param, r[k])
	}
	h, err := cache.Get(ctx, key, r)
	if err != nil {
		return nil, err
	}
	n, err := h.Native()
	if err != nil {
		return nil, err
	}
	id, ok := n[dim.ID]
	if !ok {
		return nil, errors.Errorf("Failed to retrieve dimension id from %+v", n)
	}
	return id, nil
}

// Refresh performs the specified type1 refresh
func (dim *Dimension) refresh(ctx context.Context, in Querier, q Conn, qin, qout Quoter, upd Update, s Source) error {
	points, err := s.Partition(ctx, in, qin, upd.Where, dim.allBut(upd.Where))
	if err != nil {
		return err
	}
	defer points.Close()
	// Prepare the update statement
	updater := upd.Updater(qout, dim.Table)
	rin, rout := make(Record), make(Record)
	for points.Next() {
		// Get input and fill it with the proper task
		if err := points.Scan(rin); err != nil {
			return err
		}
		if err := dim.translate(rin, rout); err != nil {
			return err
		}
		// Add update parameters
		st := updater(rout)
		if err := q.ExecContext(ctx, st.Query, st.Params); err != nil {
			return err
		}
	}
	if err := points.Err(); err != nil {
		return err
	}
	return nil
}

// key from dimension
func (dim *Dimension) key() Request {
	type2 := dim.type2()
	key := Request{
		Input:  type2,
		Output: []string{dim.ID},
		Param:  make([]interface{}, len(type2)),
		Table:  dim.Table,
		Order:  dim.Order,
	}
	key.Sort()
	return key
}

// Points of the dimension collected from the source table
func (dim *Dimension) points(ctx context.Context, q Querier, quoter Quoter, s Source) (QueryResult, error) {
	uniq, attr := make(map[string]struct{}), make(map[string]struct{})
	dim.nestedFields(uniq, attr)
	return s.Partition(ctx, q, quoter, serializeMap(uniq), serializeMap(attr))
}

// Updater for the current dimension
func (dim *Dimension) inserter(quoter Quoter) Inserter {
	hash := dim.ID
	if !dim.Hash {
		hash = ""
	}
	return quoter.Inserter(dim.Table, dim.type2(), dim.type1(), hash)
}

// Type2 enumerates all type2 attributes, including ID if it is not a hash.
func (dim *Dimension) type2() []string {
	type2 := make([]string, 0, len(dim.Type2)+len(dim.Dimensions)+1)
	type2 = append(type2, dim.Type2...)
	for name := range dim.Dimensions {
		type2 = append(type2, name)
	}
	if !dim.Hash && dim.ID != "" {
		type2 = append(type2, dim.ID)
	}
	return type2
}

// Type1 enumerates all type1 attributes
func (dim *Dimension) type1() []string {
	return dim.Type1
}

// AllBut returns all type2 and type1 atributes, but the ones included in the input field
func (dim *Dimension) allBut(exclude []string) []string {
	// Merge all type1 and t2 attributes
	keys := make(map[string]struct{})
	for _, k := range dim.type1() {
		keys[k] = struct{}{}
	}
	for _, k := range dim.type2() {
		keys[k] = struct{}{}
	}
	// Remove all keys already in exclude
	for _, k := range exclude {
		delete(keys, k)
	}
	attr := make([]string, 0, len(keys))
	for k := range keys {
		attr = append(attr, k)
	}
	return attr
}

// Translate an input record from the source table to an output record
func (dim *Dimension) translate(in, out Record) error {
	for k, s := range dim.Mapping {
		out[k] = in[s]
	}
	for k := range dim.Dimensions {
		out[k] = in[k]
	}
	if dim.task != nil {
		if err := dim.task.Execute(ioutil.Discard, out); err != nil {
			return err
		}
	}
	return nil
}

// nestedTransfer performs the transfer on each nested dimension
func (dim *Dimension) nestedTransfer(ctx context.Context, in Querier, out Connecter, qin, qout Quoter, cache Interface, s Source) error {
	wg := sync.WaitGroup{}
	errChan := make(chan error, len(dim.Dimensions))
	for _, nested := range dim.Dimensions {
		wg.Add(1)
		go func(nested Dimension) {
			defer wg.Done()
			if err := nested.Transfer(ctx, in, out, qin, qout, cache, s); err != nil {
				errChan <- err
			}
		}(nested)
	}
	wg.Wait()
	close(errChan)
	for err := range errChan {
		return err
	}
	return nil
}

// nestedLookup looks up all the dimensions of a record
func (dim *Dimension) nestedLookup(ctx context.Context, cache Interface, record Record, keys map[string]Request) (Record, error) {
	keyRecord := make(Record)
	for name, nested := range dim.Dimensions {
		// build a key record for this dimension, from the source record
		if err := nested.translate(record, keyRecord); err != nil {
			return nil, err
		}
		// look up the key record
		id, err := nested.lookup(ctx, cache, keyRecord, keys[name])
		if err != nil {
			return nil, err
		}
		// store the dimension id
		record[name] = id
	}
	return record, nil
}

// Keys for each nested dimension
func (dim *Dimension) nestedKeys() map[string]Request {
	keys := make(map[string]Request)
	for dimName, nested := range dim.Dimensions {
		keys[dimName] = nested.key()
	}
	return keys
}

// SourceFields enumerates all source fields, uniq and attr, for all nested dimensions
func (dim *Dimension) nestedFields(uniq, attr map[string]struct{}) {
	for _, k := range dim.type2() {
		if s, ok := dim.Mapping[k]; ok {
			uniq[s] = struct{}{}
		}
	}
	for _, k := range dim.type1() {
		if s, ok := dim.Mapping[k]; ok {
			attr[s] = struct{}{}
		}
	}
	for _, nested := range dim.Dimensions {
		nested.nestedFields(uniq, attr)
	}
}

// serializeMap extracts the keys from a map to a slice
func serializeMap(m map[string]struct{}) []string {
	s := make([]string, 0, len(m))
	for k := range m {
		s = append(s, k)
	}
	return s
}
