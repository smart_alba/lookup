package lookup

import (
	"encoding/base64"
	"hash/fnv"
	"math/big"
	"regexp"
	"strconv"
	"sync"
	"text/template"
	"time"

	"github.com/Masterminds/sprig"
	"github.com/pkg/errors"
	"github.com/snabb/isoweek"
)

// FuncMap replaces sprig regexp functions with "optimized" versions
// Regexps are compiled and cached, so they can be reused each time the template runs.
// It is assumed the template will run hundreds/ thousands of times; it can
// halve the executon time of the templates in that case.
func FuncMap() template.FuncMap {
	fm, c := template.FuncMap(sprig.FuncMap()), sync.Map{}
	fromMap := func(regex string) *regexp.Regexp {
		cached, ok := c.Load(regex)
		if !ok {
			cached = regexp.MustCompile(regex)
			c.Store(regex, cached)
		}
		return cached.(*regexp.Regexp)
	}
	epoch := time.Date(1900, 1, 1, 0, 0, 0, 0, time.UTC)
	fm["regexMatch"] = func(regex string, s string) bool {
		return fromMap(regex).MatchString(s)
	}
	fm["regexFindAll"] = func(regex string, s string, n int64) []string {
		return fromMap(regex).FindAllString(s, int(n))
	}
	fm["regexFind"] = func(regex string, s string) string {
		return fromMap(regex).FindString(s)
	}
	fm["regexReplaceAll"] = func(regex string, s string, repl string) string {
		return fromMap(regex).ReplaceAllString(s, repl)
	}
	fm["regexReplaceAllLiteral"] = func(regex string, s string, repl string) string {
		return fromMap(regex).ReplaceAllLiteralString(s, repl)
	}
	fm["regexSplit"] = func(regex string, s string, n int64) []string {
		return fromMap(regex).Split(s, int(n))
	}
	fm["dateFromParts"] = func(year, month, day int64) time.Time {
		if month <= 0 || day <= 0 {
			return time.Time{}
		}
		return time.Date(int(year), time.Month(month), int(day), 0, 0, 0, 0, time.UTC)
	}
	fm["isoWeek"] = func(date time.Time) int {
		if date.IsZero() {
			return 0
		}
		y, w := date.ISOWeek()
		return y*100 + w
	}
	fm["linearWeek"] = func(date time.Time) int {
		if date.IsZero() {
			return 0
		}
		return linearWeek(date, epoch)
	}
	fm["weekToDate"] = func(week int) time.Time {
		if week == 0 {
			return time.Time{}
		}
		return weekToDate(week)
	}
	fm["int"] = func(i interface{}) interface{} {
		v, err := ToInt(i, 64)
		if err != nil {
			return nil
		}
		return v
	}
	fm["float"] = func(i interface{}) interface{} {
		v, err := ToFloat(i, 64)
		if err != nil {
			return nil
		}
		return v
	}
	fm["fnv"] = func(i string) string {
		return base64.StdEncoding.EncodeToString(fnv.New128a().Sum([]byte(i)))
	}
	return fm
}

// LinearWeek returns the weeks passed since Monday Jan 1st 1900
func linearWeek(date, epoch time.Time) int {
	wday := int(date.Weekday()+6) % 7 // weekday but Monday = 0.
	diff := 3 - wday                  // diferencia hasta el jueves
	midw := date.Add(time.Duration(diff*24) * time.Hour)
	return int(midw.Sub(epoch) / (time.Duration(7*24) * time.Hour))
}

// weekToISO returns the monday of a given iso week
func weekToDate(isoWeek int) time.Time {
	return isoweek.StartTime(isoWeek/100, isoWeek%100, time.Local)
}

// ToInt coerces an interface to int64
func ToInt(v interface{}, bitSize int) (int64, error) {
	switch vt := v.(type) {
	case string:
		vi, err := strconv.ParseInt(vt, 10, bitSize)
		if err != nil {
			return 0, errors.Wrapf(err, "Failed to decode string '%s' to int", vt)
		}
		return vi, nil
	case int:
		return int64(vt), nil
	case int8:
		return int64(vt), nil
	case int16:
		return int64(vt), nil
	case int32:
		return int64(vt), nil
	case int64:
		return int64(vt), nil
	case uint:
		return int64(vt), nil
	case uint8:
		return int64(vt), nil
	case uint16:
		return int64(vt), nil
	case uint32:
		return int64(vt), nil
	case uint64:
		return int64(vt), nil
	case float32:
		return int64(vt), nil
	case float64:
		return int64(vt), nil
	case *big.Int:
		return vt.Int64(), nil
	case *big.Rat:
		if r, ok := vt.Float64(); ok {
			return int64(r), nil
		}
	case *big.Float:
		if r, a := vt.Int64(); a == big.Exact {
			return r, nil
		}
	}
	return 0, errors.Errorf("Failed to coerce unknown type %+v to int", v)
}

// ToFloat coerces an interface to float64
func ToFloat(v interface{}, bitSize int) (float64, error) {
	switch vt := v.(type) {
	case string:
		vi, err := strconv.ParseFloat(vt, bitSize)
		if err != nil {
			return 0, errors.Wrapf(err, "Failed to decode string '%s' to float", vt)
		}
		return vi, nil
	case int:
		return float64(vt), nil
	case int8:
		return float64(vt), nil
	case int16:
		return float64(vt), nil
	case int32:
		return float64(vt), nil
	case int64:
		return float64(vt), nil
	case uint:
		return float64(vt), nil
	case uint8:
		return float64(vt), nil
	case uint16:
		return float64(vt), nil
	case uint32:
		return float64(vt), nil
	case uint64:
		return float64(vt), nil
	case float32:
		return float64(vt), nil
	case float64:
		return float64(vt), nil
	case *big.Int:
		return float64(vt.Int64()), nil
	case *big.Rat:
		if r, ok := vt.Float64(); ok {
			return r, nil
		}
	case *big.Float:
		if r, a := vt.Float64(); a == big.Exact {
			return r, nil
		}
	}
	return 0, errors.Errorf("Failed to coerce unknown type %+v to float", v)
}
