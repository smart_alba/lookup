package lookup

import (
	"bytes"
	"context"
	"encoding/json"
	"io"
	"io/ioutil"
	"net/http"
	"strings"
	"sync"
	"time"

	"github.com/golang/groupcache"
	"github.com/pkg/errors"
)

// MaxResponseSize is the maximum Response size for a lookup
const MaxResponseSize = 65535

// Hit represents a lookup hit
type Hit interface {
	Native() (Record, error)
	Serialized() ([]byte, error)
}

// Interface a key in whatever source, return its value
type Interface interface {
	Get(ctx context.Context, key Request, out Record) (Hit, error)
}

// Func is a wrapper to turn a function into a Lookup object
type Func func(ctx context.Context, key Request, out Record) (Hit, error)

// Get implements Cache
func (c Func) Get(ctx context.Context, key Request, out Record) (Hit, error) {
	return c(ctx, key, out)
}

// Result returned by the lookup server
type Result struct {
	OK     bool            `json:"ok"`
	Err    string          `json:"err,omitempty"`
	Result json.RawMessage `json:"result,omitempty"`
}

// ByteHit is a []byte implementing the Hit interface
type ByteHit []byte

// Serialized implements Hit
func (b ByteHit) Serialized() ([]byte, error) {
	return []byte(b), nil
}

// Native implements interface Hit
func (b ByteHit) Native() (Record, error) {
	r := make(Record)
	if err := json.Unmarshal(b, &r); err != nil {
		return nil, errors.Wrapf(err, "Failed to decode []byte %s", string(b))
	}
	return r, nil
}

// String implements Stringer interface
func (b ByteHit) String() string {
	return string(b)
}

type cache struct {
	group *groupcache.Group
}

// Get implements Interface
func (c cache) Get(ctx context.Context, key Request, out Record) (Hit, error) {
	keyString, err := json.Marshal(key)
	if err != nil {
		return nil, err
	}
	var b []byte
	if err := c.group.Get(ctx, string(keyString), groupcache.AllocatingByteSliceSink(&b)); err != nil {
		return nil, err
	}
	return ByteHit(b), nil
}

// Cache creates a new Cache that auto-populates using the lookup
func Cache(name string, size int64, lookup Interface) Interface {
	return cache{
		groupcache.NewGroup(name, size, groupcache.GetterFunc(
			func(groupCtx groupcache.Context, key string, dest groupcache.Sink) error {
				var keyData Request
				dec := json.NewDecoder(strings.NewReader(key))
				if err := dec.Decode(&keyData); err != nil {
					return errors.Wrapf(err, "Failed to decode key %v", key)
				}
				h, err := lookup.Get(context.Background(), keyData, nil)
				if err != nil {
					return err
				}
				b, err := h.Serialized()
				if err != nil {
					return err
				}
				dest.SetBytes(b)
				return nil
			})),
	}
}

// FromDB performs a lookup against the a DB
func FromDB(db Querier, quoter Quoter) Interface {
	return Func(func(ctx context.Context, key Request, out Record) (Hit, error) {
		if out == nil {
			out = make(Record)
		}
		if err := key.IsValid(); err != nil {
			return nil, err
		}
		if err := key.Query(ctx, db, quoter, out); err != nil {
			return nil, err
		}
		return out, nil
	})
}

// FromHTTP creates a lookup func that performs the lookup against a server
func FromHTTP(url string) Func {
	client := http.Client{
		Timeout: 10 * time.Second,
		// Set the transport to allow more than 2 concurrent connections to the same server
		Transport: &http.Transport{
			MaxIdleConns:        100,
			MaxIdleConnsPerHost: 100,
		},
	}
	return Func(func(ctx context.Context, key Request, out Record) (Hit, error) {
		buf := bytes.Buffer{}
		enc := json.NewEncoder(&buf)
		if err := enc.Encode(key); err != nil {
			return nil, err
		}
		resp, err := client.Post(url, "application/json", bytes.NewReader(buf.Bytes()))
		if resp != nil && resp.Body != nil {
			defer func() {
				io.Copy(ioutil.Discard, resp.Body)
				resp.Body.Close()
			}()
		}
		if err != nil {
			return nil, err
		}
		var reply Result
		dec := json.NewDecoder(io.LimitReader(resp.Body, MaxResponseSize))
		if err := dec.Decode(&reply); err != nil {
			return nil, err
		}
		if !reply.OK {
			if reply.Err == "" {
				return nil, errors.Wrapf(io.EOF, "Key %v not found", key)
			}
			return nil, errors.Errorf("Error while looking up key: %s", reply.Err)
		}
		return ByteHit([]byte(reply.Result)), nil
	})
}

// Memoize lookup errors
func Memoize(lookup Interface) Func {
	mem := sync.Map{}
	return Func(func(ctx context.Context, key Request, out Record) (Hit, error) {
		keyByte, err := json.Marshal(key)
		if err != nil {
			return nil, err
		}
		keyString := string(keyByte)
		if c, ok := mem.Load(keyString); ok {
			return c.(Hit), nil
		}
		h, err := lookup.Get(ctx, key, nil) // Allocate a new buffer if needed
		if err != nil {
			return nil, err
		}
		mem.Store(keyString, h)
		return h, nil
	})
}

// Retry lookup for the specified number of times and duration
func Retry(cache Interface, retries int, delay time.Duration) Func {
	return Func(func(ctx context.Context, key Request, out Record) (Hit, error) {
		count := retries
		for {
			b, err := cache.Get(ctx, key, out)
			if count <= 0 || err == nil || errors.Cause(err) == io.EOF {
				return b, err
			}
			count--
			time.Sleep(delay)
		}
	})
}

// Deterministic recodes a javascript encoded Request in a deterministic manner
func Deterministic(data []byte, table string) (Request, error) {
	var key Request
	if err := json.Unmarshal(data, &key); err != nil {
		return Request{}, errors.Wrapf(err, "Failed to decode key %s", key)
	}
	if table != "" {
		key.Table = table
	}
	key.Sort()
	return key, nil
}
