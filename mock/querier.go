package mock

import (
	"context"
	"fmt"
	"log"

	"bitbucket.org/smart_alba/lookup"
	"github.com/pkg/errors"
)

// Querier mock
type Querier struct {
	// query and args are populated by ContextQuery
	Query  string
	Args   interface{}
	Result Result
	Err    error
}

// QueryContext implements Querier
func (q *Querier) QueryContext(ctx context.Context, query string, arg []interface{}, cols []string) (lookup.QueryResult, error) {
	q.Query = query
	q.Args = arg
	q.Result.columns = cols
	return &q.Result, q.Err
}

// ExecContext implements Conn
func (q *Querier) ExecContext(ctx context.Context, query string, arg []interface{}) error {
	return errors.New("NotImplemented")
}

// Connection implements Querier
func (q *Querier) Connection(ctx context.Context) (lookup.Conn, error) {
	return q, nil
}

// Close implements Conn
func (q *Querier) Close() {
}

// ID implements lookup.Quoter
func (q *Querier) ID(id string) string {
	return fmt.Sprintf("`%s`", id)
}

// Partition implements lookup.Quoter
func (q *Querier) Partition(table string, fields, partition, order []string) string {
	log.Fatal("Not implemented")
	return ""
}

// Inserter implements lookup.Quoter
func (q *Querier) Inserter(table string, unique, update []string, hash string) lookup.Inserter {
	log.Fatal("Not implemented")
	return nil
}
