package mock

import "bitbucket.org/smart_alba/lookup"

// Result mock
type Result struct {
	RColumns    []string
	RColtypes   []string
	RScan       []interface{}
	RScanned    bool
	RErrColumns error
	RErrClose   error
	RErrScan    error
	RErr        error
	columns     []string
}

// Columns implement Result interface
func (m *Result) Columns() ([]string, error) {
	if m.RErrColumns != nil {
		return nil, m.RErrColumns
	}
	return m.RColumns, nil
}

// Close implements Result interface
func (m *Result) Close() error {
	if m.RErrClose != nil {
		return m.RErrClose
	}
	return nil
}

// Next implements Result interface
func (m *Result) Next() bool {
	if m.RScan == nil && m.RErrScan == nil {
		return false
	}
	result := !m.RScanned
	m.RScanned = true
	return result
}

// Scan implements Result interface
func (m *Result) Scan(result lookup.Record) error {
	if m.RErrScan != nil {
		return m.RErrScan
	}
	for idx, k := range m.columns {
		v := m.RScan[idx]
		if b, ok := v.([]byte); ok {
			v = string(b)
		}
		result[k] = v
	}
	return nil
}

// Err implements Result interface
func (m *Result) Err() error {
	return m.RErr
}
