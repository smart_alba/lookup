package mysql

import (
	"fmt"
	"strings"
	"unicode"

	"bitbucket.org/smart_alba/lookup"
	"github.com/golang-sql/sqlexp"
)

type mysqlQuoter struct {
	sqlexp.Quoter
}

// New creates a new Quoter for Mysql Database
func New(q sqlexp.Quoter) lookup.Quoter {
	return mysqlQuoter{q}
}

// Inserter returns a function that can build statements for upserting records in the table
func (quoter mysqlQuoter) Inserter(table string, unique, update []string, hash string) lookup.Inserter {
	// Fields for insert / update
	fields := append(append([]string{}, unique...), update...)
	// Do we have to create a hashed ID?
	sqlHash := ""
	if hash != "" {
		fields = append(fields, quoter.ID(hash))
		sqlHash = quoter.sqlHash(unique)
	}
	// Prepare the insertion statement
	insert, ondup := "INSERT IGNORE INTO %s (%s) VALUES", ""
	if len(update) > 0 {
		tmp := make([]string, 0, len(update))
		for _, u := range update {
			u = quoter.ID(u)
			tmp = append(tmp, fmt.Sprintf("%s=VALUES(%s)", u, u))
		}
		insert = "INSERT INTO %s (%s) VALUES"
		ondup = fmt.Sprintf("ON DUPLICATE KEY UPDATE %s", strings.Join(tmp, ","))
	}
	insert = fmt.Sprintf(insert,
		quoter.ID(table),
		strings.Join(lookup.QuoteSlice(quoter, fields), ", "))
	// And return the updater
	return lookup.Inserter(func(rows []lookup.Record) lookup.Statement {
		params := make([]interface{}, 0, len(rows)*(len(fields)+len(unique)))
		tokens := make([]string, 0, len(rows))
		values := make([]string, 0, len(fields)+1)
		for _, row := range rows {
			// 'values' is a list of "?" or "DEFAULT(field)", if field is unique and value is null.
			values = values[:0]
			// Unique fields cannot be null. If they are, replace with "DEFAULT(field)"
			for _, k := range unique {
				if v := row[k]; v == nil {
					values = append(values, fmt.Sprintf("DEFAULT(%s)", quoter.ID(k)))
				} else {
					values = append(values, "?")
					params = append(params, v)
				}
			}
			// Non unique fields can be null
			for _, k := range update {
				values = append(values, "?")
				params = append(params, row[k])
			}
			// If there is a hash, need to add the values for the hash
			if hash != "" {
				values = append(values, sqlHash)
				// And the ID requires all non-null values to be added to params
				for _, k := range unique {
					params = append(params, row[k])
				}
			}
			tokens = append(tokens, fmt.Sprintf("(%s)", strings.Join(values, ",")))
		}
		st := lookup.Statement{
			Query: strings.Join([]string{
				insert,
				strings.Join(tokens, ","),
				ondup,
			}, " "),
			Params: params,
		}
		return st
	})
}

// sqlHash creates a SQL sentence to hash the given fields
func (quoter mysqlQuoter) sqlHash(fields []string) string {
	buf := make([]string, 0, len(fields))
	for _, f := range fields {
		buf = append(buf, fmt.Sprintf("'/', COALESCE(?,DEFAULT(%s))", quoter.ID(f)))
	}
	return fmt.Sprintf("CONV(SUBSTRING(SHA2(CONCAT(%s),'224'), 1, 16), 16, 10)",
		strings.Join(buf, ","))
}

// Partition selects the first row of each partition
func (quoter mysqlQuoter) Partition(table string, fields []string, partition []string, order []string) string {
	orderStr := ""
	if len(order) > 0 {
		orderStr = fmt.Sprintf(" ORDER BY %s DESC", strings.Join(lookup.QuoteSlice(quoter, order), " DESC,"))
	}
	flist := strings.Join(lookup.QuoteSlice(quoter, fields), ",")
	plist := strings.Join(lookup.QuoteSlice(quoter, partition), ",")
	sql := fmt.Sprintf(`
		WITH ranked_data AS (SELECT %s, ROW_NUMBER() OVER (PARTITION BY %s %s) AS rn FROM %s)
		SELECT %s FROM ranked_data WHERE rn=1
		`, flist, plist, orderStr, quoter.ID(table), flist)
	return sql
}

// ID implements sqlexp.Quoter interface
func (quoter mysqlQuoter) ID(name string) string {
	if quoter.Quoter != nil {
		return quoter.Quoter.ID(name)
	}
	runes := make([]rune, 0, len(name))
	for _, r := range name {
		if unicode.IsLetter(r) || unicode.IsDigit(r) || r == '_' {
			runes = append(runes, r)
		}
	}
	return fmt.Sprintf("`%s`", string(runes))
}
