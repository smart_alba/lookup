package mysql

import (
	"testing"

	"bitbucket.org/smart_alba/lookup"
	"github.com/google/go-cmp/cmp"
)

func TestPrepareWithoutHash(t *testing.T) {
	unique := []string{"u1", "u2"}
	update := []string{"d1", "d2"}
	table := "test"
	quoter := New(nil)
	updater := quoter.Inserter(table, unique, update, "")
	rows := []lookup.Record{
		{"u1": 5, "u2": "tt", "d1": 10.0, "d2": true},
		{"u1": 4, "u2": "ff", "d1": 1.0, "d2": false},
	}
	expected := lookup.Statement{
		Query: "INSERT INTO `test` (`u1`, `u2`, `d1`, `d2`) VALUES (?,?,?,?),(?,?,?,?) ON DUPLICATE KEY UPDATE `d1`=VALUES(`d1`),`d2`=VALUES(`d2`)",
		Params: []interface{}{
			5, "tt", 10.0, true, 4, "ff", 1.0, false,
		},
	}
	r := updater(rows)
	if !cmp.Equal(expected, updater(rows)) {
		t.Errorf("Expected %v, got %v", expected, r)
	}
}

func TestPrepareWithHash(t *testing.T) {
	unique := []string{"u1", "u2"}
	update := []string{"d1", "d2"}
	table := "test"
	quoter := New(nil)
	updater := quoter.Inserter(table, unique, update, "id")
	rows := []lookup.Record{
		{"u1": 5, "u2": "tt", "d1": 10.0, "d2": true},
		{"u1": 4, "u2": "ff", "d1": 1.0, "d2": false},
	}
	expected := lookup.Statement{
		Query: "INSERT INTO `test` (`u1`, `u2`, `d1`, `d2`, `id`) VALUES (?,?,?,?,CONV(SUBSTRING(SHA2(CONCAT('/', COALESCE(?,DEFAULT(`u1`)),'/', COALESCE(?,DEFAULT(`u2`))),'224'), 1, 16), 16, 10)),(?,?,?,?,CONV(SUBSTRING(SHA2(CONCAT('/', COALESCE(?,DEFAULT(`u1`)),'/', COALESCE(?,DEFAULT(`u2`))),'224'), 1, 16), 16, 10)) ON DUPLICATE KEY UPDATE `d1`=VALUES(`d1`),`d2`=VALUES(`d2`)",
		Params: []interface{}{
			5, "tt", 10.0, true, 5, "tt", 4, "ff", 1.0, false, 4, "ff",
		},
	}
	r := updater(rows)
	if !cmp.Equal(expected, updater(rows)) {
		t.Errorf("Expected %v, got %v", expected, r)
	}
}

func TestPrepareWithNil(t *testing.T) {
	unique := []string{"u1", "u2"}
	update := []string{"d1", "d2"}
	table := "test"
	quoter := New(nil)
	updater := quoter.Inserter(table, unique, update, "")
	rows := []lookup.Record{
		{"u1": 5, "u2": nil, "d1": 10.0, "d2": true},
		{"u1": 4, "u2": "ff", "d1": 1.0, "d2": false},
	}
	expected := lookup.Statement{
		Query: "INSERT INTO `test` (`u1`, `u2`, `d1`, `d2`) VALUES (?,DEFAULT(`u2`),?,?),(?,?,?,?) ON DUPLICATE KEY UPDATE `d1`=VALUES(`d1`),`d2`=VALUES(`d2`)",
		Params: []interface{}{
			5, 10.0, true, 4, "ff", 1.0, false,
		},
	}
	r := updater(rows)
	if !cmp.Equal(expected, updater(rows)) {
		t.Errorf("Expected %v, got %v", expected, r)
	}
}

func TestPrepareWithNilAndHash(t *testing.T) {
	unique := []string{"u1", "u2"}
	update := []string{"d1", "d2"}
	table := "test"
	quoter := New(nil)
	updater := quoter.Inserter(table, unique, update, "id")
	rows := []lookup.Record{
		{"u1": 5, "u2": "tt", "d1": 10.0, "d2": true},
		{"u1": nil, "u2": "ff", "d1": 1.0, "d2": false},
	}
	expected := lookup.Statement{
		Query: "INSERT INTO `test` (`u1`, `u2`, `d1`, `d2`, `id`) VALUES (?,?,?,?,CONV(SUBSTRING(SHA2(CONCAT('/', COALESCE(?,DEFAULT(`u1`)),'/', COALESCE(?,DEFAULT(`u2`))),'224'), 1, 16), 16, 10)),(DEFAULT(`u1`),?,?,?,CONV(SUBSTRING(SHA2(CONCAT('/', COALESCE(?,DEFAULT(`u1`)),'/', COALESCE(?,DEFAULT(`u2`))),'224'), 1, 16), 16, 10)) ON DUPLICATE KEY UPDATE `d1`=VALUES(`d1`),`d2`=VALUES(`d2`)",
		Params: []interface{}{
			5, "tt", 10.0, true, 5, "tt", "ff", 1.0, false, nil, "ff",
		},
	}
	r := updater(rows)
	if !cmp.Equal(expected, updater(rows)) {
		t.Errorf("Expected %v, got %v", expected, r)
	}
}
