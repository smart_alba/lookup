package lookup

import (
	"context"
	"database/sql"
	json "encoding/json"
	"fmt"
	"strconv"
	"strings"

	"github.com/pkg/errors"
)

// MaxSafeInt is the max integer number that can be represented in JavaScript / JSON
const MaxSafeInt = (1 << 53) - 1

// Record of a query result
type Record map[string]interface{}

// Statement with SQL query and params
type Statement struct {
	Query  string
	Params []interface{}
}

// Inserter is a func that gets a batch of rows and returns an Upsert statement
type Inserter func([]Record) Statement

// Updater is a func that gets a row and returns an Update statement
type Updater func(Record) Statement

// Quoter is a mock interface around sqlexp.Quote
type Quoter interface {
	ID(string) string
	Partition(table string, fields, partition, order []string) string
	Inserter(table string, unique, update []string, hash string) Inserter
}

// Conn is a mock interface around sql.Conn
type Conn interface {
	ExecContext(ctx context.Context, query string, arg []interface{}) error
	Close()
}

// Connecter is a mock interface around *sql.DB, for testing
type Connecter interface {
	Connection(ctx context.Context) (Conn, error)
}

// Querier is a mock interface around *sql.DB, for testing
type Querier interface {
	QueryContext(ctx context.Context, query string, arg []interface{}, cols []string) (QueryResult, error)
}

// DB is a mock interface around *sql.DB, for testing
type DB interface {
	Querier
	Connecter
}

// QueryResult is a mock interface around *sql.Rows, for testing
type QueryResult interface {
	Columns() ([]string, error)
	Close() error
	Err() error
	Next() bool
	Scan(Record) error
}

// QuoteSlice quotes all fields in a slice
func QuoteSlice(q Quoter, slice []string) []string {
	quoted := make([]string, 0, len(slice))
	for _, s := range slice {
		quoted = append(quoted, q.ID(s))
	}
	return quoted
}

// NewDB wraps a sqlx.DB in a DB interface
func NewDB(db *sql.DB) DB {
	return querier{DB: db}
}

// where struct to build where clauses
type where struct {
	Where []string
	Param []interface{}
}

// order struct to build "order by" clauses
type order struct {
	Order []string
}

type querier struct {
	*sql.DB
}

type conn struct {
	*sql.Conn
}

type queryResult struct {
	*sql.Rows
	colNames []string
	pointers []interface{}
	values   []interface{}
}

func (q querier) QueryContext(ctx context.Context, query string, arg []interface{}, cols []string) (QueryResult, error) {
	// Prepare first to keep type information (MySQL limitation...)
	prep, err := q.DB.PrepareContext(ctx, query)
	if err != nil {
		return nil, errors.Wrapf(err, "Failed to prepare query %s", query)
	}
	rows, err := prep.QueryContext(ctx, arg...)
	if err != nil {
		return nil, errors.Wrapf(err, "Failed to run query %s", query)
	}
	rowLen := len(cols)
	buffer := make([]interface{}, 2*rowLen)
	values := buffer[0:rowLen]
	pointers := buffer[rowLen : 2*rowLen]
	for idx := range pointers {
		pointers[idx] = &values[idx]
	}
	return queryResult{
		Rows:     rows,
		colNames: cols,
		pointers: pointers,
		values:   values,
	}, err
}

func (q querier) Connection(ctx context.Context) (Conn, error) {
	c, err := q.DB.Conn(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "Failed to create DB connection")
	}
	return conn{c}, nil
}

func (c conn) ExecContext(ctx context.Context, query string, arg []interface{}) error {
	if _, err := c.Conn.ExecContext(ctx, query, arg...); err != nil {
		return errors.Wrapf(err, "Failed to run query %s", query)
	}
	return nil
}

func (c conn) Close() {
	c.Conn.Close()
}

func (r queryResult) Columns() ([]string, error) {
	c, err := r.Rows.Columns()
	if err != nil {
		return nil, errors.Wrap(err, "Failed to collect result columns")
	}
	return c, nil
}

// Scan a row of results
func (r queryResult) Scan(result Record) error {
	// Neat trick to be able to read schemaless results from query
	// See https://stackoverflow.com/questions/17845619/how-to-call-the-scan-variadic-function-in-golang-using-reflection
	for idx := range r.values {
		r.values[idx] = nil
	}
	if err := r.Rows.Scan(r.pointers...); err != nil {
		return errors.Wrapf(err, "Failed to scan result row")
	}
	rowLen := len(r.colNames)
	for idx, v := range r.values {
		if idx >= rowLen {
			break
		}
		switch typed := v.(type) {
		// Since this is going to get transported over JSON, and JSON does not know
		// of integer numbers (only floats), we turn large integer values into strings,
		// so we don't miss precission.
		case int64:
			if typed > MaxSafeInt || typed < -MaxSafeInt {
				v = strconv.FormatInt(typed, 10)
			}
		case uint64:
			if typed > MaxSafeInt {
				v = strconv.FormatUint(typed, 10)
			}
		case []byte:
			v = string(typed) // assume the encoding is utf8
		}
		result[r.colNames[idx]] = v
	}
	return nil
}

// Add a (key, value) pair to a where clause
func (w *where) Add(q Quoter, key string, val interface{}) {
	if w.Param == nil {
		w.Param = make([]interface{}, 0, 8)
		w.Where = make([]string, 0, 8)
	}
	if val == nil {
		esc := q.ID(key)
		w.Where = append(w.Where, fmt.Sprintf("%s = DEFAULT(%s)", esc, esc))
		return
	}
	w.Where = append(w.Where, fmt.Sprintf("%s = ?", q.ID(key)))
	w.Param = append(w.Param, val)
}

// SQL where clause
func (w *where) SQL() string {
	return fmt.Sprintf("(%s)", strings.Join(w.Where, ") AND ("))
}

// Add a key and ascending / descending clause
func (o *order) Add(q Quoter, key string, desc bool) {
	if o.Order == nil {
		o.Order = make([]string, 0, 8)
	}
	escaped, template := q.ID(key), "%s ASC"
	if desc {
		template = "%s DESC"
	}
	o.Order = append(o.Order, fmt.Sprintf(template, escaped))
}

// SQL Order by clause
func (o *order) SQL() string {
	if len(o.Order) <= 0 {
		return ""
	}
	return fmt.Sprintf("ORDER BY %s", strings.Join(o.Order, ", "))
}

// Encap encapsulates a value in a record. For usage in templates
func (r Record) Encap(name string, value interface{}) string {
	r[name] = value
	return name
}

// Concat concatenates the current value of a record with a new string.
func (r Record) Concat(name string, tail string) string {
	r[name] = fmt.Sprintf("%s%s", r[name], tail)
	return name
}

// Decap decapsulates a value in a record. For usage in templates
func (r Record) Decap(name string) interface{} {
	return r[name]
}

// Native implements interface Hit
func (r Record) Native() (Record, error) {
	return r, nil
}

// Serialized implements interface Hit
func (r Record) Serialized() ([]byte, error) {
	b, err := json.Marshal(r)
	if err != nil {
		return nil, errors.Wrapf(err, "Failed to serialize record %+v", r)
	}
	return b, nil
}
