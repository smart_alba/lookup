//go:generate easyjson -omit_empty $GOFILE

package lookup

import (
	"context"
	json "encoding/json"
	"fmt"
	"io"
	"log"
	"sort"
	"strings"

	"github.com/pkg/errors"
)

// Sorting order
//easyjson:json
type Sorting struct {
	Field string `json:"field" yaml:"field"`
	Desc  bool   `json:"desc" yaml:"desc"`
}

// Request for a database lookup
//easyjson:json
type Request struct {
	Table  string        `json:"table"`
	Input  []string      `json:"input"`
	Param  []interface{} `json:"param"`
	Output []string      `json:"output"`
	Order  []Sorting     `json:"order"`
}

// MustJSON serializes the request, Fatals if it fails
func (r Request) MustJSON() []byte {
	buf, err := json.Marshal(r)
	if err != nil {
		log.Fatal("Failed to encode ", r, ", err: ", err)
	}
	return buf
}

// SQL query to run for this lookup
func (r Request) sql(q Quoter) (string, []interface{}) {
	fields := make([]string, 0, len(r.Output))
	for _, v := range r.Output {
		fields = append(fields, q.ID(v))
	}
	w := where{}
	for idx, input := range r.Input {
		w.Add(q, input, r.Param[idx])
	}
	o := order{}
	for _, v := range r.Order {
		o.Add(q, v.Field, v.Desc)
	}
	return fmt.Sprintf("SELECT %s FROM %s WHERE %s %s LIMIT 1",
		strings.Join(fields, ", "),
		q.ID(r.Table),
		w.SQL(), o.SQL()), w.Param
}

// Query the requested data against the database
func (r Request) Query(ctx context.Context, conn Querier, q Quoter, out Record) error {
	sql, params := r.sql(q)
	rows, err := conn.QueryContext(ctx, sql, params, r.Output)
	if err != nil {
		return errors.Wrapf(err, "Failed to run SQL query %s with params %v", sql, params)
	}
	defer rows.Close()
	cols, err := rows.Columns()
	if err != nil {
		return errors.Wrapf(err, "Failed to get query result columns for query %s", sql)
	}
	if len(cols) != len(r.Output) {
		return errors.Wrapf(err, "Returned less columns than requested for query %s", sql)
	}
	rowsToGet := 1
	for rows.Next() {
		if rowsToGet <= 0 {
			continue
		}
		rowsToGet--
		if err = rows.Scan(out); err != nil {
			return err
		}
	}
	if err := rows.Err(); err != nil {
		return errors.Wrap(err, "Failure after iterating on rows")
	}
	if rowsToGet > 0 {
		return errors.Wrap(io.EOF, "No results retrieved")
	}
	return nil
}

// IsValid checks the request can generate valid and deterministic SQL
func (r *Request) IsValid() error {
	if len(r.Input) <= 0 {
		return errors.Errorf("At least an input filter must be specified for %+v", r)
	}
	if len(r.Input) != len(r.Param) {
		return errors.Errorf("Size of Input and Params arrays must be equal in %+v", r)
	}
	if len(r.Output) <= 0 {
		return errors.Errorf("At least an output column must be selected in %+v", r)
	}
	return nil
}

// Sort the items in the Request so it can be serialized in a deterministic way
func (r *Request) Sort() {
	sort.Sort(r)
	sort.Strings(r.Output)
}

// Len implements sort.Interface
func (r *Request) Len() int {
	return len(r.Input)
}

// Less implements sort.Interface
func (r *Request) Less(i, j int) bool {
	return strings.Compare(r.Input[i], r.Input[j]) < 1
}

// Swap implements sort.Interface
func (r *Request) Swap(i, j int) {
	r.Input[i], r.Input[j] = r.Input[j], r.Input[i]
	r.Param[i], r.Param[j] = r.Param[j], r.Param[i]
}
