-- MySQL dump 10.13  Distrib 8.0.12, for Linux (x86_64)
--
-- Host: localhost    Database: test
-- ------------------------------------------------------
-- Server version	8.0.12

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8mb4 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

CREATE DATABASE IF NOT EXISTS test;
USE test;

--
-- Table structure for table `d_simple_entidad`
--

DROP TABLE IF EXISTS `d_simple_entidad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `d_simple_entidad` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `d_simple_entidad`
--

LOCK TABLES `d_simple_entidad` WRITE;
/*!40000 ALTER TABLE `d_simple_entidad` DISABLE KEYS */;
/*!40000 ALTER TABLE `d_simple_entidad` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `d_simple_periodo`
--

DROP TABLE IF EXISTS `d_simple_periodo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `d_simple_periodo` (
  `id` bigint(20) NOT NULL,
  `ejercicio` int(11) NOT NULL DEFAULT -1,
  `mes` int(11) NOT NULL DEFAULT -1,
  `dia` int(11) NOT NULL DEFAULT -1,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ejercicio` (`ejercicio`,`mes`,`dia`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `d_simple_periodo`
--

LOCK TABLES `d_simple_periodo` WRITE;
/*!40000 ALTER TABLE `d_simple_periodo` DISABLE KEYS */;
/*!40000 ALTER TABLE `d_simple_periodo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `f_simple`
--

DROP TABLE IF EXISTS `f_simple`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `f_simple` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_entidad` int(11) NOT NULL,
  `id_periodo` bigint(20) NOT NULL,
  `medida` decimal(20,6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_entidad` (`id_entidad`,`id_periodo`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `f_simple`
--

LOCK TABLES `f_simple` WRITE;
/*!40000 ALTER TABLE `f_simple` DISABLE KEYS */;
/*!40000 ALTER TABLE `f_simple` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_simple`
--

DROP TABLE IF EXISTS `t_simple`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `t_simple` (
  `id` varchar(255) DEFAULT NULL,
  `ejercicio` int(11) DEFAULT NULL,
  `mes` int(11) DEFAULT NULL,
  `id_entidad` int(11) DEFAULT NULL,
  `nombre_entidad` varchar(100) DEFAULT NULL,
  `medida` decimal(20,6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_simple`
--

LOCK TABLES `t_simple` WRITE;
/*!40000 ALTER TABLE `t_simple` DISABLE KEYS */;
INSERT INTO `t_simple` VALUES ('1',2018,8,1,'entidad_1',100.000000),('1',2018,7,2,'entidad_2',200.000000),('3',2018,8,2,'entidad_2',250.000000);
/*!40000 ALTER TABLE `t_simple` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-08-16 16:56:12