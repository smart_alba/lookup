# DB Lookup server

This application implements a small key-value cache server in front of an SQL Database.

- Keys include a table name and lists of column names - column values.
- Values are a CSV-like representation of chosen output columns.

The cache has no expiration or eviction; it is assumed that once a key is populated, its value will always remain the same. This cache is intended for OLAP use cases, not OLTP use cases.

## Configuration

The application accepts the following config parameters:

- `-name`: A name for the cache, used internally. Has no particular effect currently.
- `-size`: The size for the cache, in megabytes.
- `-address`: The address (`IP:port`or just `:port`) to listen on for REST requests
- `-peers`: Coma separated list of HTTP addresses for a set of peers to build a distributed cache (see [groupcache](https://github.com/golang/groupcache)).
- `-driver`: The golang DB driver to connect to the database. Currently, only mysql is compiled in. If you need a different driver, you'll have to add it to main.go and recompile.
- `-dsn`: The database connection string in golang format, e.g. `user:password@tcp(IP:port)/database`.
- `-c`: Path to a config file while you can set any of the above parameters. Config file format can be json, yaml or toml.

The same params can be set through the environment, using environment variables with the `LOOKUP_` prefix (e.g. `LOOKUP_SIZE`, `LOOKUP_DSN`, etc).

All log is sent to stdout, intended for usage inside a docker container.

## Usage

Just send a POST request to the /lookup route with a json body with the following schema:

```json
{
  "table": "table_name",
  "input": ["input_name_1", "input_name_2", ...],
  "param": ["input_value_1", "input_value_2", ...],
  "output": ["output_name_1", "output_name_2", ...],
  "order": [
    { "field": "column_name",
      "desc": false },
    { "field": "column_name",
      "desc": true },
  ]
}
```

The response will be an `application/json` object with the following format:

```json
{
  "ok":     true|false,
  "err":    "Error message if any, otherwise empty",
  "result": {
    "<output_name_1>": "<output_value_1>",
    "<output_name_2>": "<output_value_2>",
    ...
  }
}
```

Only the first row in the database that matches the input criteria will be returned. You can use the `order` field to specify how different rows with the same input values should be sorted, before piking the first one.
