package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/signal"
	"strings"
	"time"

	"bitbucket.org/smart_alba/lookup"
	"bitbucket.org/smart_alba/lookup/test"
	_ "github.com/go-sql-driver/mysql"
	"github.com/golang/groupcache"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/pkg/errors"
)

// MaxRequestSize is an arbitrary limit on the maximum size of a request
const MaxRequestSize = 8192

// Options coming from command line, environment or config file
type Options struct {
	Name    string   `yaml:"name" help:"Service name"`
	Size    uint     `yaml:"size" help:"Cache size (megabytes)"`
	Address string   `yaml:"port" help:"Listening address, golang format (e.g. \":8080\", \"localhost:8080\")"`
	Peers   []string `yaml:"peers" help:"Comma-separated list of peer addresses (e,g, http://a.b.c.d:8080,http://w.x.y.z:8080)"`
	Driver  string   `yaml:"driver" help:"Name of go database driver"`
	DSN     string   `yaml:"dsn" help:"Database DSN in golang format, user:password@tcp(host:port)/db"`
}

// Defaults returns a set of config options
func Defaults() Options {
	return Options{
		Name:    "lookup",
		Size:    64,
		Address: ":8080",
		Driver:  "mysql",
		DSN:     "",
	}
}

func init() {
	lookup.InitConfig(Defaults())
}

type stopFlush struct {
	w io.Writer
}

func (w stopFlush) Write(b []byte) (int, error) {
	return w.w.Write(b)
}

func performLookup(group lookup.Interface, w http.ResponseWriter, r *http.Request) (lookup.Hit, error) {
	if r.Body == nil {
		return nil, errors.New("Empty request body")
	}
	defer func() {
		io.Copy(ioutil.Discard, r.Body)
		r.Body.Close()
	}()
	data, err := ioutil.ReadAll(io.LimitReader(r.Body, MaxRequestSize))
	if err != nil {
		return nil, err
	}
	table := r.URL.Query().Get("table")
	key, err := lookup.Deterministic(data, table)
	if err != nil {
		return nil, err
	}
	return group.Get(r.Context(), key, nil)
}

func lookupHandler(group lookup.Interface) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		result := lookup.Result{}
		w.Header().Set("Content-Type", "application/json; charset=utf-8")
		var b []byte
		h, err := performLookup(group, w, r)
		if err == nil {
			b, err = h.Serialized()
		}
		if err != nil {
			if errors.Cause(err) != io.EOF {
				result.Err = err.Error()
			}
			w.WriteHeader(http.StatusNotFound)
		} else {
			result.Result = json.RawMessage(b)
			result.OK = true
			w.WriteHeader(http.StatusOK)
		}
		enc := json.NewEncoder(w)
		enc.Encode(result)
	})
}

func watchSignals(server *http.Server, wait chan struct{}) {
	defer close(wait)
	sigint := make(chan os.Signal, 1)
	signal.Notify(sigint, os.Interrupt, os.Kill)
	<-sigint
	if err := server.Shutdown(context.Background()); err != nil {
		log.Printf("HTTP server Shutdown: %+v", err)
	}
}

func main() {
	// Load config and prime connections
	opts := Defaults()
	if err := lookup.LoadConfig(&opts, "LOOKUP"); err != nil {
		log.Fatal("Failed to read configuration options, ", err)
	}
	db, quoter := test.Must(opts.Driver, opts.DSN)
	// Set up the routing
	group := lookup.Cache(opts.Name, int64(opts.Size)<<20, lookup.FromDB(db, quoter))
	router := mux.NewRouter()
	router.Path("/lookup").Methods("POST").Handler(handlers.LoggingHandler(os.Stdout, lookupHandler(group)))
	router.PathPrefix("/").Handler(handlers.LoggingHandler(os.Stdout, http.DefaultServeMux))
	// Prepare the pool
	hosts := make([]string, 1, len(opts.Peers)+1)
	template := "http://%s"
	if strings.HasPrefix(opts.Address, ":") {
		template = "http://localhost%s"
	}
	hosts[0] = fmt.Sprintf(template, opts.Address)
	hosts = append(hosts, opts.Peers...)
	pool := groupcache.NewHTTPPool(hosts[0])
	pool.Set(hosts...)
	// Run the service!
	wait := make(chan struct{})
	h := http.Server{
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 10 * time.Second,
		Addr:         opts.Address,
		Handler:      router,
	}
	go watchSignals(&h, wait)
	if err := h.ListenAndServe(); err != nil {
		if err != http.ErrServerClosed {
			log.Fatal("HTTP Listen and Serve: ", err)
		}
		<-wait
	}
}
