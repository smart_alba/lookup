package lookup

import (
	"context"
)

// TableSource is a source that simply reads from a table
type TableSource struct {
	// Table to read facts from
	Table string `yaml:"table"`
	// Order to sort the mapping table to have most recent entries first
	Order []string `yaml:"order"`
}

// Cube describes an star table with dimensional links
type Cube struct {
	// Source to read facts from
	Source TableSource `yaml:"source"`
	// Root of the Star
	Root Dimension `yaml:"root"`
}

// Partition implements Source
func (s TableSource) Partition(ctx context.Context, q Querier, quoter Quoter, uniq, attr []string) (QueryResult, error) {
	src := make([]string, 0, len(uniq)+len(attr))
	par := make([]string, 0, len(uniq))
	out := make([]string, 0, cap(src))
	for _, k := range uniq {
		src = append(src, k)
		par = append(par, k)
		out = append(out, k)
	}
	for _, k := range attr {
		src = append(src, k)
		out = append(out, k)
	}
	sql := quoter.Partition(s.Table, src, par, s.Order)
	return q.QueryContext(ctx, sql, nil, out)
}
