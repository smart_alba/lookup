package test

import (
	"context"
	"encoding/json"
	"fmt"
	"sort"
	"strings"

	"bitbucket.org/smart_alba/lookup"
	"github.com/google/go-cmp/cmp"
	"github.com/pkg/errors"
)

func testRequest() lookup.Request {
	return lookup.Request{
		Table:  "tab",
		Input:  []string{"i1", "i2", "i3"},
		Param:  []interface{}{nil, "value2", 5},
		Output: []string{"o1", "o2"},
		Order: []lookup.Sorting{
			{Field: "sort1"},
			{Field: "sort2", Desc: true},
		},
	}
}

// Expectation encodes the expected result for a table
type expectation struct {
	Table  string
	Fields []string
	Expect []lookup.Record
}

// newExp defines what results are expected in a db query
func newExp(table string, fields []string, expect []lookup.Record) expectation {
	return expectation{Table: table, Fields: fields, Expect: expect}
}

func (e expectation) Check(querier lookup.Querier, quoter lookup.Quoter) error {
	sql := fmt.Sprintf("SELECT %s FROM %s",
		strings.Join(lookup.QuoteSlice(quoter, e.Fields), ","),
		quoter.ID(e.Table))
	rows, err := querier.QueryContext(context.Background(), sql, nil, e.Fields)
	if err != nil {
		return err
	}
	defer rows.Close()
	got := make([]lookup.Record, 0, 100)
	for rows.Next() {
		r := make(lookup.Record)
		if err := rows.Scan(r); err != nil {
			return err
		}
		got = append(got, r)
	}
	if err := rows.Err(); err != nil {
		return err
	}
	if len(got) != len(e.Expect) {
		return errors.Errorf("Got %d replies for table %s, expected %d replies", len(got), e.Table, len(e.Expect))
	}
	gt, err := sortRecords(got)
	if err != nil {
		return err
	}
	et, err := sortRecords(e.Expect)
	if err != nil {
		return err
	}
	if !cmp.Equal(et, gt) {
		return errors.Errorf("Expected %+v, got %+v, diff: %s", et, gt, cmp.Diff(et, gt))
	}
	return nil
}

func sortRecords(records []lookup.Record) ([]string, error) {
	result := make([]string, 0, len(records))
	// First, sort each record by field name
	names := make([]string, 0, 16)
	row := make([]interface{}, 0, 16)
	for _, r := range records {
		names, row = names[:0], row[:0]
		for k := range r {
			names = append(names, k)
		}
		sort.Strings(names)
		for _, k := range names {
			row = append(row, r[k])
		}
		// Then, turn each record into a textual representation, to sort them
		b, err := json.Marshal(row)
		if err != nil {
			return nil, err
		}
		result = append(result, string(b))
	}
	sort.Strings(result)
	return result, nil
}
