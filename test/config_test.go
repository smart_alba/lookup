package test

import (
	"os"
	"testing"

	"bitbucket.org/smart_alba/lookup"
	"github.com/google/go-cmp/cmp"
)

// Options coming from command line, environment or config file
type Options struct {
	Name    string   `yaml:"name"`
	Size    uint     `yaml:"size"`
	Address string   `yaml:"port"`
	Peers   []string `yaml:"peers"`
	Driver  string   `yaml:"driver"`
	DSN     string   `yaml:"dsn"`
	Test    bool     `yaml:"test"`
	Empty   string   `yaml:"empty"`
	Int     int      `yaml:"int"`
}

// Defaults returns a set of config options
func Defaults() Options {
	return Options{
		Name:    "lookup",
		Size:    64,
		Address: ":8080",
		Peers: []string{
			"http://peer1:8080",
			"http://peer2:8080",
		},
		Driver: "mysql",
		Empty:  "",
		Int:    10,
		DSN:    "",
	}
}

func init() {
	lookup.InitConfig(Defaults())
}

func TestConfig(t *testing.T) {
	// Populated config file
	f, _ := os.Create("config.yaml")
	f.Write([]byte("---\ndsn: test.value\nport: :9000\n"))
	f.Close()
	// Empty config file
	defer os.Remove("config.yaml")
	e, _ := os.Create("empty.yaml")
	e.Write([]byte("---"))
	e.Close()
	defer os.Remove("empty.yaml")
	// Tests must be run in specific order, because once flags.Parse recognizes a flag,
	// it won't clean it afterwards even if we overwrite os.Args
	t.Run("defaults are kept", subTestDefaultsAreKept)
	t.Run("config file is read", subTestConfigFileIsRead)
	t.Run("slices are replaced", subTestSlicesAreReplaced)
	t.Run("defaults don't overwrite config", subTestDefaultsDontOverwrite)
	t.Run("environment takes precedence", subTestEnvironmentTakesPrecedence)
	t.Run("flags take precedence", subTestFlagsTakePrecedence)
}

func subTestDefaultsAreKept(t *testing.T) {
	oldArgs := os.Args
	defer func() { os.Args = oldArgs }()
	os.Args = []string{"lookup", "-c", "empty.yaml"}
	opts := Defaults()
	if err := lookup.LoadConfig(&opts, "LOOKUP"); err != nil {
		t.Fatal(err)
	}
	if !cmp.Equal(opts, Defaults()) {
		t.Fatal("Expected ", Defaults(), ", got", opts)
	}
}

func subTestConfigFileIsRead(t *testing.T) {
	oldArgs := os.Args
	defer func() { os.Args = oldArgs }()
	os.Args = []string{"lookup", "-c", "config.yaml"}
	opts := Defaults()
	if err := lookup.LoadConfig(&opts, "LOOKUP"); err != nil {
		t.Fatal(err)
	}
	if opts.DSN != "test.value" {
		t.Fail()
	}
}

func subTestSlicesAreReplaced(t *testing.T) {
	oldArgs := os.Args
	defer func() { os.Args = oldArgs }()
	os.Args = []string{"lookup", "-peers", "http://peer3:8080"}
	opts := Defaults()
	if err := lookup.LoadConfig(&opts, "LOOKUP"); err != nil {
		t.Fatal(err)
	}
	expected := []string{"http://peer3:8080"}
	if !cmp.Equal(opts.Peers, expected) {
		t.Fatal("Expected ", expected, ", got", opts.Peers)
	}
}

func subTestDefaultsDontOverwrite(t *testing.T) {
	oldArgs := os.Args
	defer func() { os.Args = oldArgs }()
	os.Args = []string{"lookup", "-c", "config.yaml"}
	opts := Defaults()
	if err := lookup.LoadConfig(&opts, "LOOKUP"); err != nil {
		t.Fatal(err)
	}
	if opts.Address != ":9000" {
		t.Error("Expected address = :9000, got ", opts.Address)
	}
}

func subTestEnvironmentTakesPrecedence(t *testing.T) {
	oldArgs := os.Args
	defer func() { os.Args = oldArgs }()
	os.Args = []string{"lookup", "-c", "config.yaml"}
	os.Setenv("LOOKUP_DSN", "env.value")
	opts := Defaults()
	if err := lookup.LoadConfig(&opts, "LOOKUP"); err != nil {
		t.Fatal(err)
	}
	if opts.DSN != "env.value" {
		t.Fail()
	}
}

func subTestFlagsTakePrecedence(t *testing.T) {
	oldArgs := os.Args
	defer func() { os.Args = oldArgs }()
	os.Args = []string{"lookup", "-c", "config.yaml", "-dsn", "cli.value"}
	os.Setenv("LOOKUP_DSN", "env.value")
	opts := Defaults()
	if err := lookup.LoadConfig(&opts, "LOOKUP"); err != nil {
		t.Fatal(err)
	}
	if opts.DSN != "cli.value" {
		t.Fail()
	}
}
