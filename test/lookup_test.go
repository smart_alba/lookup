package test

import (
	"context"
	"testing"

	"bitbucket.org/smart_alba/lookup"
	"bitbucket.org/smart_alba/lookup/mock"
	"github.com/google/go-cmp/cmp"
	"github.com/pkg/errors"
)

func TestCollectorSingleResult(t *testing.T) {
	querier := &mock.Querier{
		Result: mock.Result{
			RColumns:  []string{"o1"},
			RColtypes: []string{"float"},
			RScan:     []interface{}{1000.05},
		},
	}
	req, expected := testRequest(), map[string]interface{}{"o1": 1000.05}
	req.Output = []string{"o1"}
	if err := compare(querier, querier, req, expected); err != nil {
		t.Fatal(err)
	}
}

func TestCollectorMultipleResults(t *testing.T) {
	querier := &mock.Querier{
		Result: mock.Result{
			RColumns:  []string{"o1", "o2"},
			RColtypes: []string{"string", "double"},
			RScan:     []interface{}{"ok", 1000.05},
		},
	}
	req, expected := testRequest(), map[string]interface{}{"o1": "ok", "o2": 1000.05}
	if err := compare(querier, querier, req, expected); err != nil {
		t.Fatal(err)
	}
}

func compare(q lookup.Querier, quoter lookup.Quoter, r lookup.Request, expected map[string]interface{}) error {
	collector := lookup.FromDB(q, quoter)
	b, err := collector.Get(context.Background(), r, nil)
	if err != nil {
		return err
	}
	result, err := b.Native()
	if err != nil {
		return err
	}
	if !cmp.Equal(result, lookup.Record(expected)) {
		return errors.Errorf("Expected '%s' , got '%s'", expected, result)
	}
	return nil
}

func BenchmarkDeterministic(b *testing.B) {
	// run the Fib function b.N times
	buf := testRequest().MustJSON()
	for n := 0; n < b.N; n++ {
		if _, err := lookup.Deterministic(buf, ""); err != nil {
			b.Fatal(err)
		}
	}
}
