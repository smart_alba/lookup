package test

import (
	"context"
	"database/sql"
	"log"

	"bitbucket.org/smart_alba/lookup"
	"bitbucket.org/smart_alba/lookup/mysql"
	"github.com/golang-sql/sqlexp"
)

const (
	driverMysql = "mysql"
)

// Must create a lookup.DB and lookup.Quoter from a database DSN
func Must(driver, dsn string) (lookup.DB, lookup.Quoter) {
	switch driver {
	case driverMysql:
	default:
		log.Fatalf("Database driver %s is not supported", driver)
	}
	// Open and ping the database
	db, err := sql.Open(driver, dsn)
	if err != nil {
		log.Fatalf("Failed to connect to database: %+v", err)
	}
	if err := db.Ping(); err != nil {
		log.Fatalf("Failed to ping the database: %+v", err)
	}
	// Try to get the quoter
	dbQuoter, err := sqlexp.QuoterFromDriver(db.Driver(), context.Background())
	if err != nil {
		log.Print("DB does not provide Quoter, failing over to Mock quoting")
		dbQuoter = nil
	}
	var quoter lookup.Quoter
	switch driver {
	case driverMysql:
		quoter = mysql.New(dbQuoter)
	}
	return lookup.NewDB(db), quoter
}
