package test

import (
	"context"
	"io"
	"testing"

	"bitbucket.org/smart_alba/lookup"
	"bitbucket.org/smart_alba/lookup/mock"
	"github.com/google/go-cmp/cmp"
	"github.com/pkg/errors"
)

func TestSimpleSQL(t *testing.T) {
	q := &mock.Querier{
		Result: mock.Result{
			RColumns:  []string{"o1", "o2"},
			RColtypes: []string{"int", "string"},
			RScan:     []interface{}{5, "res2"},
		},
	}
	k, r := testRequest(), make(lookup.Record)
	if err := k.Query(context.Background(), q, q, r); err != nil {
		t.Fatal(err)
	}
	// Check the query matches
	query := "SELECT `o1`, `o2` FROM `tab` WHERE (`i1` = DEFAULT(`i1`)) AND (`i2` = ?) AND (`i3` = ?) ORDER BY `sort1` ASC, `sort2` DESC LIMIT 1"
	if q.Query != query {
		t.Error("Expected query", query, ", got", q.Query)
	}
	// Check the args match
	args := []interface{}{
		k.Param[1], // params[0] is nil
		k.Param[2], // params[0] is nil
	}
	queryArgs, ok := q.Args.([]interface{})
	if !ok {
		t.Fatal("queryArgs is not a slice,", q.Args)
	}
	if !cmp.Equal(args, queryArgs) {
		t.Error("Expected args", args, ", got", q.Args)
	}
	// Check the result match
	expected := lookup.Record(map[string]interface{}{"o1": 5, "o2": "res2"})
	if !cmp.Equal(r, expected) {
		t.Fatal("Expected result", expected, ", got", r)
	}
}

func TestByteSliceIsConvertedToString(t *testing.T) {
	q := &mock.Querier{
		Result: mock.Result{
			RColumns:  []string{"o1", "o2"},
			RColtypes: []string{"int", "string"},
			RScan:     []interface{}{5, []byte("res2")},
		},
	}
	k, r := testRequest(), make(lookup.Record)
	if err := k.Query(context.Background(), q, q, r); err != nil {
		t.Fatal(err)
	}
	// Check the result match
	expected := lookup.Record(map[string]interface{}{"o1": 5, "o2": "res2"})
	if !cmp.Equal(r, expected) {
		t.Fatal("Expected result", expected, ", got", r)
	}
}

func TestNoResultYieldsEOF(t *testing.T) {
	q := &mock.Querier{
		Result: mock.Result{
			RColtypes: []string{"int", "string"},
			RColumns:  []string{"o1", "o2"},
		},
	}
	k := testRequest()
	err := k.Query(context.Background(), q, q, make(lookup.Record))
	if errors.Cause(err) != io.EOF {
		t.Fail()
	}
}
