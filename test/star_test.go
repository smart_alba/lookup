package test

import (
	"context"
	"os"
	"testing"

	"bitbucket.org/smart_alba/lookup"
	_ "github.com/go-sql-driver/mysql"
)

// You will need to spin up a test database to run these tests.
// Use the schema in schemas/*.sql, e.g.

// docker run --rm -e MYSQL_ROOT_PASSWORD=pass -v "$PWD/../schemas:/docker-entrypoint-initdb.d:ro" -p 3306:3306 mysql:latest

const Driver = "mysql"
const DSN = "root:pass@tcp(localhost:3306)/test"

var cache lookup.Interface
var querier lookup.DB
var quoter lookup.Quoter

func TestMain(m *testing.M) {
	querier, quoter = Must(Driver, DSN)
	cache = lookup.FromDB(querier, quoter)
	os.Exit(m.Run())
}

// TestStar runs a Star.Upsert and checks the output of dimension and fact tables
func do(t *testing.T, cube lookup.Cube, expect []expectation) {
	cube.Root.Compile("root")
	if err := cube.Root.Transfer(context.Background(), querier, querier, quoter, quoter, cache, cube.Source); err != nil {
		t.Fatal(err)
	}
	for _, e := range expect {
		if err := e.Check(querier, quoter); err != nil {
			t.Error(err)
		}
	}
}

func TestSimpleCube(t *testing.T) {
	cube := lookup.Cube{
		Source: lookup.TableSource{
			Table: "t_simple",
			Order: []string{"ejercicio", "mes"},
		},
		Root: lookup.Dimension{
			Table: "f_simple",
			Type1: []string{"medida"},
			Mapping: map[string]string{
				"medida": "medida",
			},
			Dimensions: map[string]lookup.Dimension{
				"id_entidad": {
					Table: "d_simple_entidad",
					Type1: []string{"nombre"},
					ID:    "id",
					Hash:  false,
					Mapping: map[string]string{
						"id":     "id_entidad",
						"nombre": "nombre_entidad",
					},
				},
				"id_periodo": {
					Table: "d_simple_periodo",
					Type2: []string{"ejercicio", "mes", "dia"},
					ID:    "id",
					Hash:  false,
					Task: `
						{{ .Encap "dia" 0 }}
						{{ add (mul .ejercicio 10000) (mul .mes 100) .dia  | .Encap "id" }}
						`,
					Mapping: map[string]string{
						"ejercicio": "ejercicio",
						"mes":       "mes",
					},
				},
			},
		},
	}
	expect := []expectation{
		newExp("d_simple_entidad", []string{"id", "nombre"},
			[]lookup.Record{
				{"id": 1, "nombre": "entidad_1"},
				{"id": 2, "nombre": "entidad_2"},
			}),
		newExp("d_simple_periodo", []string{"id", "ejercicio", "mes", "dia"},
			[]lookup.Record{
				{"id": 20180700, "ejercicio": 2018, "mes": 7, "dia": 0},
				{"id": 20180800, "ejercicio": 2018, "mes": 8, "dia": 0},
			}),
		newExp("f_simple", []string{"id_entidad", "id_periodo", "medida"},
			[]lookup.Record{
				{"id_entidad": 1, "id_periodo": 20180800, "medida": "100.000000"},
				{"id_entidad": 2, "id_periodo": 20180800, "medida": "250.000000"},
				{"id_entidad": 2, "id_periodo": 20180700, "medida": "200.000000"},
			}),
	}
	do(t, cube, expect)
}
