package lookup

import (
	"fmt"
	"strings"
)

// Update identified which fields to update when a type2 field changes
type Update struct {
	Update []string `yaml:"update"`
	Where  []string `yaml:"where"`
}

// Updater creates a new Updater
func (u Update) Updater(quoter Quoter, table string) Updater {
	lenUpdate := len(u.Update)
	insert := make([]string, 0, lenUpdate)
	for _, k := range u.Update {
		insert = append(insert, fmt.Sprintf("%s=?", quoter.ID(k)))
	}
	setstr := strings.Join(insert, ", ")
	params := make([]interface{}, 0, lenUpdate+len(u.Where))
	table = quoter.ID(table)
	// Prepare the "where" statement
	w := where{
		Where: make([]string, 0, len(u.Where)),
		Param: make([]interface{}, 0, len(u.Where)),
	}
	return Updater(func(r Record) Statement {
		params = params[:0]
		for _, k := range u.Update {
			params = append(params, r[k])
		}
		// Add where parameters
		w.Where = w.Where[:0]
		w.Param = w.Param[:0]
		for _, k := range u.Where {
			w.Add(quoter, k, r[k])
		}
		// Build the statement
		return Statement{
			Query:  fmt.Sprintf("UPDATE %s SET %s WHERE %s", table, setstr, w.SQL()),
			Params: append(params, w.Param...),
		}
	})
}
