package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"os"

	"bitbucket.org/smart_alba/lookup"
	"bitbucket.org/smart_alba/lookup/test"
	_ "github.com/go-sql-driver/mysql"
	"gopkg.in/yaml.v2"
)

// Options coming from command line, environment or config file
type Options struct {
	Lookup    string `yaml:"lookup" help:"URL of the lookup server, e.g. http://lookup.service:8080/lookup"`
	SrcDriver string `yaml:"srcDriver" help:"Name of source go database driver"`
	SrcDSN    string `yaml:"srcDSN" help:"Source Database DSN in golang format, user:password@tcp(host:port)/db"`
	DstDriver string `yaml:"dstDriver" help:"Name of destination go database driver"`
	DstDSN    string `yaml:"dstDSN" help:"Destination Database DSN in golang format, user:password@tcp(host:port)/db"`
	Test      bool   `yaml:"test" help:"Test mode, just load the star and compile the templates"`
}

// Defaults returns a set of config options
func Defaults() Options {
	return Options{
		Lookup:    "http://lookup.service:8080/lookup",
		SrcDriver: "mysql",
		DstDriver: "mysql",
	}
}

func init() {
	lookup.InitConfig(Defaults())
}

func main() {
	// Load config and prime connections
	opts := Defaults()
	if err := lookup.LoadConfig(&opts, "UPSERT"); err != nil {
		log.Fatal("Failed to read configuration options, ", err)
	}

	// Prepare db and cache connections
	var srcQuoter, dstQuoter lookup.Quoter
	var srcDB lookup.Querier
	var dstDB lookup.Connecter
	var cache lookup.Interface
	if !opts.Test {
		srcDB, srcQuoter = test.Must(opts.SrcDriver, opts.SrcDSN)
		dstDB, dstQuoter = test.Must(opts.DstDriver, opts.DstDSN)
		cache = lookup.FromHTTP(opts.Lookup)
		log.Print("Setup complete, loading star files")
	}

	// Load star topologies
	args := flag.Args()
	if len(args) <= 0 {
		log.Fatal("At least one template file must be provided in the command line")
	}
	cubes := make([]lookup.Cube, 0, len(args))
	for _, arg := range args {
		f, err := os.Open(arg)
		if err != nil {
			log.Fatalf("Could not open template file %s", arg)
		}
		current := lookup.Cube{}
		dec := yaml.NewDecoder(f)
		if err := dec.Decode(&current); err != nil {
			f.Close()
			log.Fatalf("Failed to import template file %s: %+v", arg, err)
		}
		cubes = append(cubes, current)
		f.Close()
		log.Printf("Star file %s loaded", arg)
	}

	// Upsert each star
	ctx := context.Background()
	for idx, s := range cubes {
		if err := s.Root.Compile(fmt.Sprintf("cube%d", idx)); err != nil {
			log.Fatalf("Failed to compile: %+v", err)
		}
		log.Printf("Cube file number %d compiled, upserting", idx)
		if opts.Test {
			log.Println("Test mode, skipping upsert. Star data:")
			yaml.NewEncoder(os.Stdout).Encode(s)
			continue
		}
		if err := s.Root.Transfer(ctx, srcDB, dstDB, srcQuoter, dstQuoter, cache, s.Source); err != nil {
			log.Fatalf("Failed to upsert: %+v", err)
		}
		log.Printf("Star file %d completed", idx)
	}
}
